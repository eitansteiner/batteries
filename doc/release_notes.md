- 0.33.0 (2023/05/19)
    - Added Conan options for build variants that affect ABI:
        - `header_only`
        - `glog_support`
        - `protobuf_support`
        - `asserts_enabled`
        - `shared`
    - Added `<batteries/stacktrace.hpp>`, which includes boost
      stacktrace and suppresses spurious GCC warnings in the boost
      code.
    - Added a runtime check system to `<batteries/config.hpp>` that
      computes a "feature [bit-]mask" which captures the build
      variant; this is done for each compilation unit that includes
      `config.hpp` (using static init of a `bool` in an anonymous
      namespace) and compared to a global, canonical feature mask.  In
      this way, we can now detect bugs where different libraries were
      built with different Batteries feature settings and halt the
      program before undefined behavior ensues.
    - Added new macros to control optimization suppression in the code:
        - `BATT_GCC_BEGIN_OPTIMIZE(setting)` - setting must be a string literal, e.g. "O0"
        - `BATT_GCC_END_OPTIMIZE()` - reverts the optimization level set by `BATT_GCC_BEGIN_OPTIMIZE`
        - `BATT_BEGIN_NO_OPTIMIZE()` - begins a section in which optimization is completely disabled
        - `BATT_END_NO_OPTIMIZE()` - ends a section begun by `BATT_BEGIN_NO_OPTIMIZE`
    - Enabling `BATT_ASSERT` statements can now be done independent of
      the `NDEBUG` define, using `BATT_ASSERTS_ENABLED`, which is
      controlled by the Conan config via the `asserts_enabled` option,
      which can be `None` (default), `True`, or `False`.  If
      `BATT_ASSERTS_ENABLED` is not defined (i.e., `asserts_enabled`
      is `None`), then asserts are only enabled when `NDEBUG` is
      defined, like before.
    - Added support for `std::ostream operator<<` to `batt::LogLevel` (`<batteries/status.hpp>`)
    - Fixed silent ABI breakage bug in
      `batt::detail::NotOkStatusWrapper` that causes aborts at runtime
      when some libraries are compiled with `BATT_GLOG_AVAILABLE` and
      some without.
    - Fixed an overly aggressive check in `batt::Task`

- 0.31.3 (2023/05/03)
    - Fixed bug in TakeNSource that causes hang after limit is reached
      (instead of returning batt::StatusCode::kEndOfStream).

- 0.31.0 (2023/05/03)
    - Added `batt::SingleBufferSource`.

- 0.30.0 (2023/04/13)
    - Fix #4 (Added `std::ostream& operator<<(std::ostream&, batt::Watch<T>)`)
    - Fix #5 (Deadlock if a failed BATT_CHECK generates another failed check when trying to print the message)
    - Fix #6 (Added `batt::Watch<T>::reset()`)

- 0.29.0 (2023/03/31)
    - Add `batt::seq::zip`.

- 0.28.5 (2023/03/17)
    - Add doc comment for `batt::getenv_as`.

- 0.28.4 (2023/03/17)
    - Added `batt::Optional::or_else(Fn)` to match `std::optional`.
    - Changed `batt::require_fail_thread_default_log_level()` to return `batt::Optional<batt::LogLevel>&` instead of `batt::LogLevel&`, and made it default to the _current_ global setting when it is `batt::None`.

- 0.28.3 (2023/03/13)
    - Fix bug that prevented the use of `batt::Future<T>::await`.

- 0.27.1 (2023/03/08)
    - Fixed typo in case\_of.hpp doc.
    - Added support for silent conversion of `batt::StatusOr<batt::NoneType>` to `batt::Status`.

- 0.27.0 (2023/03/07)
    - Added `BATT_CHECK_TYPE(type, expr)` to `<batteries/static_assert.hpp>`:
        - At runtime, evaluates to just `expr`, but adds a compile-time assertion that the type of `expr` is `type`.

- 0.26.0 (2023/03/04)
    - Added `batt::Task::DeferStart{bool}` option to suppress automatic Task starting; also:
        - Added member fn `batt::Task::is_started()`
        - Added member fn `batt::Task::start()`
        - Added class `batt::Task::Options`

- 0.25.3 (2023/03/02)
    - Fixed many missing functions/operators for `batt::Optional<T&>` partial specialization.

- 0.25.0 (2023/02/23)
    - Added `batt::Task::await_signal` (moved from "upper layers").

- 0.24.6 (2023/02/22)
    - Added support for MacOS/Clang build pipelines
    - Added `batt::Task::spawn()`
    - Added `batt::NoDestruct<T>`

- 0.23.1 (2023/02/06)
    - Fixed bug in script/run-with-docker.sh
    - Added xxd to docker images

- 0.22.1 (2023/01/27)
    - Added `batt::DefaultInitialized`, a type that implicitly converts to any default-constructible type.
    - Added a `step` parameter (type `batt::TransferStep &`, batteries/async/fetch.hpp) that allows callers to determine which step failed when `batt::transfer_chunked_data` returns an error.

- 0.21.0 (2023/01/25)
    - Added `batt::register_error_category` to allow applications to specify how `error_code` values with custom `error_category` objects should be converted to `batt::Status`.
    - Upgraded boost to 1.81.0
    - Upgraded gtest to 1.13.0

- 0.20.1 (2023/01/18)
    - Added `batt::transfer_chunked_data` (in batteries/async/fetch.hpp).

- 0.19.8 (2023/01/11)
    - Upgrade to Conan 1.56 (from 1.53).

- 0.19.6 (2022/12/19)
    - Fixed build error with `batt::to_status(std::error_code)`

- 0.19.5 (2022/12/16)
    - Fixed crash bug caused by accidental implicit conversion from error_code enum values to batt::Status.

- 0.19.2 (2022/12/16)
    - Fixed test regression by updating the expected error status to match the boost::system::error_code value.

- 0.19.1 (2022/12/16)
    - Added accurate translation of more boost::system::error_code values into batt::Status.

- 0.19.0 (2022/12/14)
    - Added `batt::Latch<T>::set_error` to explicitly set the Latch to an error status for better readability.
    - Added new header batteries/async/fetch.hpp with:
        - `batt::fetch_chunk`: blocking API for no-copy input streams with an async_fetch method
        - `batt::BasicScopedChunk<Stream>`: automatically calls consume on fetched data at scope exit

- 0.18.1 (2022/12/09)
    - Added `batt::PinnablePtr<T>`.

- 0.18.0 (2022/12/07)
    - Added `batt::Channel<T>::async_read`, along with unit tests for the async methods of `batt::Channel<T>`.

- 0.17.1 (2022/12/02)
    - Added `batt::Runtime::reset()` and `batt::Runtime::is_halted()` so that downstream libraries that use a Google Test global environment to start/stop batt::Runtime can restart the default thread pool, for GTEST_REPEAT/--gtest_repeat testing.

- 0.16.2 (2022/11/30)
    - Fixed a broken link in the release notes.

- 0.16.1 (2022/11/30)
    - Added release notes (this document) to the doc pages.

- 0.16.0 (2022/11/30)
    - Added clamp min/max functions to [batt::Watch][battwatch], to atomically enforce upper/lower bounds on the watched value.
