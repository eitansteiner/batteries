#
# Copyright 2021-2023 Anthony Paul Astolfi
#
import os

from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps
from conan.tools.build import can_run


VERBOSE = os.getenv('VERBOSE') and True or False


class BatteriesTestConan(ConanFile):
    generators = "CMakeDeps", "CMakeToolchain"
    settings = "os", "compiler", "build_type", "arch"


    def requirements(self):
        self.requires(self.tested_reference_str)


    def build(self):
        cmake = CMake(self)
        cmake.verbose = VERBOSE
        cmake.configure()
        cmake.build()


    def layout(self):
        cmake_layout(self)


    def test(self):
        if can_run(self):
            cmd = os.path.join(self.cpp.build.bindir, "example")
            self.run(cmd, env="conanrun")
