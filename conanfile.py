##=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++
# Copyright 2022-2023 Anthony Paul Astolfi
#
from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps
from conan.tools.files import save, copy

import os, sys, platform

VERBOSE = os.getenv('VERBOSE') and True or False
CONAN_2 = False


class BatteriesConan(ConanFile):
    name = "batteries"

    #------ --- -- -  -  -   -
    # version is set automatically from Git tags - DO NOT SET IT HERE
    #------ --- -- -  -  -   -

    license = "Apache Public License 2.0"

    author = "Tony Astolfi <tastolfi@gmail.com>"

    url = "https://github.com/tonyastolfi/batteries.git"

    description = "C++ Essentials left out of std:: and boost::"

    settings = "os", "compiler", "build_type", "arch"

    options = {
        "header_only": [True, False],
        "with_glog": [True, False],
        "with_protobuf": [True, False],
        "with_asserts": [None, True, False],
        "shared": [False],
    }

    default_options = {
        "header_only": False,
        "with_glog": True,
        "with_protobuf": True,
        "with_asserts": None,
        "shared": False,
    }

    build_policy = "missing"

    exports_sources = [
        "src/CMakeLists.txt",
        "src/batteries.hpp",
        "src/batteries/*.hpp",
        "src/batteries/*/*.hpp",
        "src/batteries/**/*.hpp",
        "src/batteries/*.ipp",
        "src/batteries/*/*.ipp",
        "src/batteries/**/*.ipp",
        "src/batteries/*.cpp",
        "src/batteries/**/*.cpp",
        "script/*.sh",
        "script/**/.sh",
        "script/*.py",
        "script/**/.py",
    ]

    #+++++++++++-+-+--+----- --- -- -  -  -   -


    def _get_cxx_flags(self):
        yield "-D_GNU_SOURCE"

        if self.options.header_only:
            yield "-DBATT_HEADER_ONLY=1"
        else:
            yield "-DBATT_HEADER_ONLY=0"

        if self.options.with_glog:
            yield "-DBATT_WITH_GLOG=1"
        else:
            yield "-DBATT_WITH_GLOG=0"

        if self.options.with_protobuf:
            yield "-DBATT_WITH_PROTOBUF=1"
        else:
            yield "-DBATT_WITH_PROTOBUF=0"

        if self.options.with_asserts == True:
            yield "-DBATT_WITH_ASSERTS=1"
        elif self.options.with_asserts == False:
            yield "-DBATT_WITH_ASSERTS=0"



    def _get_requires(self):
        yield "boost/1.81.0"
        yield "libbacktrace/cci.20210118"

        if platform.system() == 'Linux':
            yield "libunwind/1.6.2"

        if self.options.with_glog:
            yield "glog/0.6.0"

        if self.options.with_protobuf:
            yield "protobuf/3.21.9"


    def set_version(self):
        #==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
        # Import the Batteries utility module.
        #
        script_dir = os.path.join(os.path.dirname(__file__), 'script')
        sys.path.append(script_dir)

        import batt

        batt.VERBOSE = VERBOSE
        self.version = batt.get_version(no_check_conan=True)
        batt.verbose(f'VERSION={self.version}')
        #
        #+++++++++++-+-+--+----- --- -- -  -  -   -


    def configure(self):
        self.options["gtest"].shared = False
        self.options["boost"].shared = False


    def requirements(self):
        if CONAN_2:
            self.test_requires("gtest/1.13.0")
        else:
            self.requires("gtest/1.13.0")

        for dependency_name in self._get_requires():
            self.requires(dependency_name,
                          visible=True,
                          transitive_headers=True,
                          transitive_libs=True)


    def layout(self):
        cmake_layout(self, src_folder="src")


    def generate(self):
        # Write the current options to a .cmake file
        #
        opts_file = os.path.join(self.build_folder, "batteries_options.cmake")
        save(self, opts_file,
             '\n'.join([f"add_definitions(\"{flag}\")"
                        for flag in self._get_cxx_flags()]))

        tc = CMakeToolchain(self, generator='Ninja')
        tc.variables["BATT_HEADER_ONLY"] = self.options.header_only
        tc.variables["BATT_WITH_GLOG"] = self.options.with_glog
        tc.variables["BATT_WITH_PROTOBUF"] = self.options.with_protobuf
        tc.variables["BATT_WITH_ASSERTS"] = self.options.with_asserts
        tc.generate()

        deps = CMakeDeps(self)
        deps.generate()


    def build(self):
        cmake = CMake(self)
        cmake.verbose = VERBOSE
        cmake.configure()
        cmake.build()


    def package(self):
        src_build = self.build_folder
        src_include = os.path.join(self.source_folder, ".")
        dst_licenses = os.path.join(self.package_folder, "licenses")
        dst_include = os.path.join(self.package_folder, "include")
        dst_lib = os.path.join(self.package_folder, "lib")
        dst_bin = os.path.join(self.package_folder, "bin")

        copy(self, "LICENSE", src=self.source_folder, dst=dst_licenses)
        copy(self, pattern="*.hpp", src=src_include, dst=dst_include)
        copy(self, pattern="*.ipp", src=src_include, dst=dst_include)
        copy(self, pattern="*.a", src=src_build, dst=dst_lib, keep_path=False)
        copy(self, pattern="*.so", src=src_build, dst=dst_lib, keep_path=False)
        copy(self, pattern="*.lib", src=src_build, dst=dst_lib, keep_path=False)
        copy(self, pattern="*.dll", src=src_build, dst=dst_bin, keep_path=False)
        copy(self, pattern="*.dylib", src=src_build, dst=dst_lib, keep_path=False)

        cmake = CMake(self)
        cmake.configure()
        cmake.install()


    def package_info(self):
        self.cpp_info.cxxflags = list(self._get_cxx_flags())

        if platform.system() == 'Linux':
            self.cpp_info.system_libs = ["dl"]

        if not self.options.header_only:
            self.cpp_info.libs = ["batteries"]


    def package_id(self):
        if self.info.options.header_only:
            self.info.clear()
