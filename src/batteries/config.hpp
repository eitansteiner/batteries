//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_CONFIG_HPP
#define BATTERIES_CONFIG_HPP

//----- --- -- -  -  -   -
// IMPORTANT: This is the only batteries header that is allowed to be included from config.hpp!
//
#include <batteries/stacktrace.hpp>
//
//----- --- -- -  -  -   -

#include <bitset>
#include <exception>
#include <iostream>

#if __cplusplus < 201703L
#error Batteries requires C++17 or later!
#endif

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
#ifndef BATT_HEADER_ONLY
#define BATT_HEADER_ONLY 1
#endif

#if BATT_HEADER_ONLY
#define BATT_INLINE_IMPL inline
#else
#define BATT_INLINE_IMPL
#endif

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
#define BATT_SEQ_SPECIALIZE_ALGORITHMS 0

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
/** \brief Define this preprocessor symbol to enable optional support for Google Log (GLOG).
 *
 *  NOTE: when using Conan to install Batteries in a downstream project, this shouldn't be defined directly;
 *  instead add the following to the downstream `conanfile.py`:
 *
 *  ```python
 *  def configure(self):
 *      ...
 *      self.options["batteries"].with_glog = True
 *  ```
 */
#ifndef BATT_WITH_GLOG
#define BATT_WITH_GLOG 0
#endif  // BATT_WITH_GLOG

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
/** \brief Define this preprocessor symbol to enable optional support for Google Protocol Buffers (protobuf).
 *
 *  NOTE: when using Conan to install Batteries in a downstream project, this shouldn't be defined directly;
 *  instead add the following to the downstream `conanfile.py`:
 *
 *  ```python
 *  def configure(self):
 *      ...
 *      self.options["batteries"].with_protobuf = True
 *  ```
 */
#ifndef BATT_WITH_PROTOBUF
#define BATT_WITH_PROTOBUF 0
#endif  // BATT_WITH_PROTOBUF

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
#if defined(__GNUC__) && !defined(__clang__)

#define BATT_COMPILER_IS_GCC 1
#define BATT_IF_GCC(expr) expr

// This causes a lot of false positives, so disable.
//
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"

#else

#define BATT_COMPILER_IS_GCC 0
#define BATT_IF_GCC(expr)

#endif

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
#if defined(__clang__)

#define BATT_COMPILER_IS_CLANG 1
#define BATT_IF_CLANG(expr) expr

#else

#define BATT_COMPILER_IS_CLANG 0
#define BATT_IF_CLANG(expr)

#endif

//+++++++++++-+-+--+----- --- -- -  -  -   -
#ifdef __APPLE__
#define BATT_PLATFORM_IS_APPLE 1
#else
#undef BATT_PLATFORM_IS_APPLE
#endif

//+++++++++++-+-+--+----- --- -- -  -  -   -
#ifdef __linux__
#define BATT_PLATFORM_IS_LINUX 1
#else
#undef BATT_PLATFORM_IS_LINUX
#endif

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
/** \brief Enables BATT_ASSERTs explicitly (instead of inferring from -DNDEBUG)
 *
 *  NOTE: when using Conan to install Batteries in a downstream project, this shouldn't be defined directly;
 *  instead add the following to the downstream `conanfile.py`:
 *
 *  ```python
 *  def configure(self):
 *      ...
 *      self.options["batteries"].with_asserts = True
 *  ```
 */
#ifndef BATT_WITH_ASSERTS
#ifdef NDEBUG
#define BATT_WITH_ASSERTS 0
#else  // NDEBUG
#define BATT_WITH_ASSERTS 1
#endif  // NDEBUG
#endif  // BATT_WITH_ASSERTS

//#=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++

namespace batt {

namespace features {

/** \brief The list of build-time features that can potentially affect ABI.
 *
 * Each of these should have a section in this file (see above) that describes the feature in more detail.
 */
enum Feature {
    kHeaderOnly = 0,
    kSeqSpecializeAlgorithms,
    kWithGlog,
    kWithProtobuf,
    kCompilerGCC,
    kCompilerClang,
    kWithAsserts,

    // Not a feature - used to check that we have enough bits in the mask int type.
    //
    kNumFeatures,
};

using mask_type = unsigned long long;

// Make sure that the int type is big enough.
//
static_assert(features::kNumFeatures <= (sizeof(mask_type) * 8));

/** \brief Computes and returns the build-time feature mask for the current configuration.
 */
inline constexpr mask_type get_feature_mask()
{
    constexpr mask_type enabled = 1;

    mask_type mask = 0;

    if (BATT_HEADER_ONLY) {
        mask |= (enabled << features::kHeaderOnly);
    }
    if (BATT_SEQ_SPECIALIZE_ALGORITHMS) {
        mask |= (enabled << features::kSeqSpecializeAlgorithms);
    }
    if (BATT_WITH_GLOG) {
        mask |= (enabled << features::kWithGlog);
    }
    if (BATT_WITH_PROTOBUF) {
        mask |= (enabled << features::kWithProtobuf);
    }
    if (BATT_COMPILER_IS_GCC) {
        mask |= (enabled << features::kCompilerGCC);
    }
    if (BATT_COMPILER_IS_CLANG) {
        mask |= (enabled << features::kCompilerClang);
    }
    if (BATT_WITH_ASSERTS) {
        mask |= (enabled << features::kWithAsserts);
    }

    return mask;
}

/** \brief Captures the normative feature mask plus the stack trace where the global singleton instance of
 * this type was initialized.
 */
struct State {
    boost::stacktrace::stacktrace init_trace;
    const mask_type mask = get_feature_mask();
};

/** \brief Computes and returns the normative feature mask for the program.
 */
inline const State& get_global_state()
{
    static const State state;
    return state;
}

/** \brief Returns true if the computed feature mask for the current compilation unit is the same as the
 * global mask; otherwise prints a diagnostic message and panics.
 */
inline bool check_local_feature_state()
{
    const State& global_state = features::get_global_state();

    if (get_feature_mask() != global_state.mask) {
        std::cerr << "FATAL: feature mask does not match!\n\nfile:" << __FILE__
                  << ", mask=" << std::bitset<features::kNumFeatures>{features::get_feature_mask()}
                  << "\n\nglobal mask=" << std::bitset<features::kNumFeatures>{global_state.mask}
                  << "\n\nglobal state initialized at: \n\n"
                  << global_state.init_trace << "\n"
                  << std::endl;

        std::terminate();
    }
    return true;
}

}  // namespace features

namespace {

/** \brief The computed feature mask for the current compilation unit.
 */
[[maybe_unused]] const bool feature_mask_check_for_module = features::check_local_feature_state();

}  // namespace

}  // namespace batt

#endif  // BATTERIES_CONFIG_HPP
