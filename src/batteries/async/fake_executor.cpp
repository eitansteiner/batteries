//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/fake_executor.hpp>
//
#include <batteries/async/fake_executor_impl.hpp>

#endif  // !BATT_HEADER_ONLY
