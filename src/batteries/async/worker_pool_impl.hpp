//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_WORKER_POOL_IMPL_HPP
#define BATTERIES_ASYNC_WORKER_POOL_IMPL_HPP

#include <batteries/config.hpp>
//
#include <batteries/cpu_align.hpp>
#include <batteries/env.hpp>
#include <batteries/logging.hpp>

#include <batteries/async/worker_pool.hpp>

#include <vector>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ WorkerPool& WorkerPool::default_pool()
{
    static WorkerPool* pool_ = [] {
        static const usize cpu_count = std::thread::hardware_concurrency();

#ifdef __linux__
        static const bool is_linux = true;
#else
        static const bool is_linux = false;
#endif

        static const ThreadPoolConfig config = {
            .thread_count = getenv_as<usize>("BATT_WORKER_POOL_DEFAULT_SIZE").value_or(cpu_count),
            .cpu_group_size = getenv_as<usize>("BATT_WORKER_POOL_CPU_GROUP_SIZE").value_or(2),
            .first_cpu = getenv_as<usize>("BATT_WORKER_POOL_FIRST_CPU").value_or(0),
            .pin = getenv_as<bool>("BATT_WORKER_POOL_PIN_CPU").value_or(is_linux),
        };

        // These are intentionally leaked to prevent shutdown issues.
        //
        auto* threads = new std::vector<std::thread>;
        auto* io = new std::vector<std::unique_ptr<boost::asio::io_context>>;
        auto* pool = new WorkerPool;

        for (usize i = 0; i < config.thread_count; ++i) {
            io->emplace_back(std::make_unique<boost::asio::io_context>());
            io->back()->get_executor().on_work_started();
            threads->emplace_back([p_io = io->back().get(), i] {
                BATT_CHECK_OK(pin_thread_i_of_config(i, config, "WorkerPool::default_pool().worker"));
                p_io->run();
            });
            pool->workers_.emplace_back(std::make_unique<Worker>(io->back()->get_executor()));
        }

        return pool;
    }();

    return *pool_;
}

}  // namespace batt

#endif  // BATTERIES_ASYNC_WORKER_POOL_IMPL_HPP
