//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_BACKOFF_IMPL_HPP
#define BATTERIES_ASYNC_BACKOFF_IMPL_HPP

#include <batteries/config.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ ExponentialBackoff ExponentialBackoff::with_default_params()
{
    ExponentialBackoff p;

    p.max_attempts = 40;
    p.initial_delay_usec = 10;
    p.backoff_factor = 2;
    p.backoff_divisor = 1;
    p.max_delay_usec = 250 * 1000;  // 250ms

    return p;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void update_retry_state(RetryState& state, const ExponentialBackoff& policy)
{
    if (state.n_attempts >= policy.max_attempts) {
        state.should_retry = false;
        return;
    }

    state.should_retry = true;
    state.n_attempts += 1;
    state.prev_delay_usec = state.next_delay_usec;
    if (state.n_attempts == 1) {
        state.next_delay_usec = policy.initial_delay_usec;
    } else {
        state.next_delay_usec = std::min(
            policy.max_delay_usec, state.prev_delay_usec * policy.backoff_factor / policy.backoff_divisor);
    }
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_BACKOFF_IMPL_HPP
