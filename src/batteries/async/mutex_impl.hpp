//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_MUTEX_IMPL_HPP
#define BATTERIES_ASYNC_MUTEX_IMPL_HPP

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void MutexBase::acquire_impl() const
{
    const u64 my_ticket = next_ticket_.fetch_add(1);
    StatusOr<u64> latest_ticket = current_ticket_.get_value();
    BATT_CHECK_OK(latest_ticket);

    // This is OK since it will probably take something like 100 years to wrap.  We should be so lucky!
    //
    while (latest_ticket.ok() && *latest_ticket < my_ticket) {
        latest_ticket = current_ticket_.await_not_equal(*latest_ticket);
    }
    BATT_CHECK_EQ(*latest_ticket, my_ticket);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void MutexBase::release() const
{
    current_ticket_.fetch_add(1);
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_MUTEX_IMPL_HPP
