#pragma once
#ifndef BATTERIES_ASYNC_SLICE_WORK_IPP
#define BATTERIES_ASYNC_SLICE_WORK_IPP

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Iter>
inline /*explicit*/ WorkSlicePlan::WorkSlicePlan(const WorkSliceParams& params, const Iter& first,
                                                 const Iter& last) noexcept
    : WorkSlicePlan{params, get_input_size(first, last)}
{
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_SLICE_WORK_IPP
