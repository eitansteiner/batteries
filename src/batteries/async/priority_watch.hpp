//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_PRIORITY_WATCH_HPP
#define BATTERIES_ASYNC_PRIORITY_WATCH_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/async/handler.hpp>
#include <batteries/int_types.hpp>
#include <batteries/optional.hpp>
#include <batteries/seq/loop_control.hpp>
#include <batteries/status.hpp>

#include <boost/container/static_vector.hpp>

#include <array>
#include <bitset>
#include <thread>

namespace batt {

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// TODO [tastolfi 2023-04-16]
//  - This code needs to be as fast as is reasonably possible
//  - Optimizations to try:
//    - Convert as much as possible to branch-free style:
//      - left/right symmetric cases should become index + children array[2]
//      - use branch prediction hints
//      - statically alloc a small array for heaps smaller than some number; use this
//        in place of the pointers to decrease cache misses
//      - generalized left/right processing should reduce total code size which will also help with cache
//        locality
//      - cache and maintain a bottom item pointer, so we don't have to do a log(N) traversal down a path to
//        find it each time
//      - change CHECKs to ASSERTs, perhaps through a template-based policy injection (so we can verify during
//        testing, but disable for production code, even in RelWithDebInfo builds.
//      - for items with the same `order` value, group at a single heap location, repurposing the 'child'
//        pointers as double-linked list pointers
//
//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

namespace detail {

template <typename IntT>
class PriorityHeap;

template <typename IntT>
class PriorityHeapItem
{
   public:
    friend class PriorityHeap<IntT>;

    using order_type = IntT;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    explicit PriorityHeapItem(IntT order) noexcept : order_{order}
    {
    }

    PriorityHeapItem() = default;

    PriorityHeapItem(const PriorityHeapItem&) = delete;
    PriorityHeapItem& operator=(const PriorityHeapItem&) = delete;

    IntT order() const noexcept
    {
        return this->order_;
    }

    bool is_linked() const noexcept
    {
        return this->children_[0] || this->children_[1];
    }

   private:
    IntT order_ = IntT{};
    std::array<PriorityHeapItem*, 2> children_ = {nullptr, nullptr};
};

template <typename IntT>
class PriorityHeapItemList
{
   public:
    using Item = PriorityHeapItem<IntT>;

    void insert(Item* item)
    {
        //this->head_.children_[0]->children_[1] = item;
        //this->head_.children_[1]->children_[0] = item;
        //item->children_ = this->head_->children_;
        item->children_[0] = &this->head_;
        this->head_.children_[1] = item;
    }

   private:
    Item head_;
};

template <typename IntT>
class PriorityHeap
{
   public:
    using Self = PriorityHeap;
    using Item = PriorityHeapItem<IntT>;

    static constexpr usize kStackSize = sizeof(usize) * (8 /*bits per byte*/);
    using StackVec = boost::container::static_vector<Item**, kStackSize>;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    __attribute__((always_inline)) static Item** swap_child(Item** parent, usize i,
                                                            const std::array<Item*, 2>& children) noexcept
    {
        const usize j = i ^ 0x1;

        (*parent)->children_ = children[i]->children_;
        children[i]->children_[i] = (*parent);
        children[i]->children_[j] = children[j];
        (*parent) = children[i];

        return &children[i]->children_[i];
    }

    __attribute__((always_inline)) static Item** swap_left(Item** parent,
                                                           const std::array<Item*, 2>& children) noexcept
    {
        return swap_child(parent, 0, children);
    }

    __attribute__((always_inline)) static Item** swap_right(Item** parent,
                                                            const std::array<Item*, 2>& children) noexcept
    {
        return swap_child(parent, 1, children);
    }

    __attribute__((always_inline)) static i32 depth_upper_bound(usize path) noexcept
    {
        constexpr i32 bits = sizeof(usize) * 8;
        return bits - __builtin_clzll(path) - 1;
    }

    __attribute__((always_inline)) static usize get_top_mask(usize path) noexcept
    {
        return usize{1} << Self::depth_upper_bound(path);
    }

    template <typename Fn>
    __attribute__((always_inline)) static void path_to_bottom(usize size, Fn&& fn) noexcept
    {
        const usize path = size + 1;
        const usize top_mask = Self::get_top_mask(path);

        for (usize mask = top_mask >> 1; mask != 0; mask >>= 1) {
            bool go_left = ((path & mask) == 0);
            fn(go_left);
        }
    }

    template <typename Fn>
    __attribute__((always_inline)) static void path_from_bottom(usize size, Fn&& fn) noexcept
    {
        const usize path = size + 1;
        const usize top_mask = Self::get_top_mask(path);

        for (usize mask = 1; mask != top_mask; mask <<= 1) {
            bool go_left = ((path & mask) == 0);
            if (seq::run_loop_fn(fn, go_left) == seq::LoopControl::kBreak) {
                break;
            }
        }
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    PriorityHeap() = default;
    PriorityHeap(const PriorityHeap&) = delete;
    PriorityHeap& operator=(const PriorityHeap&) = delete;
    ~PriorityHeap() = default;

    PriorityHeap(PriorityHeap&& other) noexcept
        : top_{other.top_}
        , size_{other.size_}
        , min_value_{other.min_value_}
    {
        other.top_ = nullptr;
        other.size_ = 0;
        other.min_value_ = None;
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    bool empty() const noexcept
    {
        return !this->top_;
    }

    usize size() const noexcept
    {
        return this->size_;
    }

    const Optional<IntT>& min_value() const noexcept
    {
        return this->min_value_;
    }

    const Item* top() const noexcept
    {
        return this->top_;
    }

    void insert(Item* item) noexcept
    {
        item->children_.fill(nullptr);

        auto on_scope_exit = batt::finally([&] {
            this->min_value_ = this->top_->order_;
            this->size_ += 1;
        });

        if (!this->top_) {
            this->top_ = item;
            return;
        }

        StackVec stack{&this->top_};

        // First insert at the bottom of the heap...
        //
        Self::path_to_bottom(this->size_, [&stack](bool go_left) {
            Item** parent = stack.back();
            BATT_CHECK_NOT_NULLPTR((*parent));

            if (go_left) {
                stack.push_back(&(*parent)->children_[0]);
            } else {
                stack.push_back(&(*parent)->children_[1]);
            }
        });
        *(stack.back()) = item;

        // ...then back-track, fixing the heap
        // property.
        //
        Self::path_from_bottom(this->size_, [&stack](bool go_left) {
            stack.pop_back();

            Item** parent = stack.back();
            const std::array<Item*, 2> children = (*parent)->children_;

            if (go_left) {
                if (BATT_HINT_TRUE(children[0]->order_ < (*parent)->order_)) {
                    Self::swap_left(parent, children);
                } else {
                    return seq::LoopControl::kBreak;
                }
            } else {
                if (BATT_HINT_TRUE(children[1]->order_ < (*parent)->order_)) {
                    Self::swap_right(parent, children);
                } else {
                    return seq::LoopControl::kBreak;
                }
            }

            return seq::LoopControl::kContinue;
        });
    }

    Item* remove() noexcept
    {
        // Base case: already empty.
        //
        if (!this->top_) {
            return nullptr;
        }

        auto on_scope_exit = batt::finally([&] {
            if (this->top_) {
                this->min_value_ = this->top_->order_;
            } else {
                this->min_value_ = None;
            }
        });

        this->size_ -= 1;

        Item* const removed = this->top_;

        // Base case: 2->1, 1->0.
        //
        if (!this->top_->children_[1]) {
            this->top_ = removed->children_[0];
            removed->children_[0] = nullptr;
            return removed;
        }

        // Find the bottom item.
        //
        Item** parent = &this->top_;
        Item* bottom = this->top_;

        Self::path_to_bottom(this->size_, [&parent, &bottom](bool go_left) {
            if (go_left) {
                parent = &bottom->children_[0];
                bottom = bottom->children_[0];
            } else {
                parent = &bottom->children_[1];
                bottom = bottom->children_[1];
            }
        });

        BATT_CHECK_NOT_NULLPTR(bottom);
        BATT_CHECK_EQ(bottom->children_[0], nullptr);
        BATT_CHECK_EQ(bottom->children_[1], nullptr);

        *parent = nullptr;

        std::swap(bottom->children_[0], this->top_->children_[0]);
        std::swap(bottom->children_[1], this->top_->children_[1]);
        this->top_ = bottom;

        parent = &this->top_;
        for (;;) {
            BATT_CHECK_NOT_NULLPTR(*parent);

            std::array<Item*, 2> children = (*parent)->children_;

            if (!children[0]) {
                BATT_CHECK_EQ(children[1], nullptr);
                break;
            }
            if (!children[1]) {
                if (!(children[0]->order_ < (*parent)->order_)) {
                    break;
                }
                parent = Self::swap_left(parent, children);
                continue;
            }
            if (BATT_HINT_TRUE(children[0]->order_ < (*parent)->order_ ||
                               children[1]->order_ < (*parent)->order_)) {
                if (children[0]->order_ < children[1]->order_) {
                    parent = Self::swap_left(parent, children);
                } else {
                    parent = Self::swap_right(parent, children);
                }
                continue;
            }
            break;
        }

        return removed;
    }

    [[nodiscard]] bool check_invariants(StackVec* stack, usize* count = nullptr)
    {
        BATT_CHECK_NOT_NULLPTR(stack);

        usize count_ = 0;
        if (!count) {
            count = &count_;
        }

        bool at_top = false;

        if (stack->empty()) {
            stack->push_back(&this->top_);
            at_top = true;
        }

        Item* const parent = *stack->back();
        if (!parent) {
            return true;
        }
        *count += 1;
        if (parent->children_[0]) {
            stack->push_back(&parent->children_[0]);
            if (parent->order_ > parent->children_[0]->order_) {
                return false;
            }
            if (!this->check_invariants(stack, count)) {
                return false;
            }
            stack->pop_back();
        }
        if (parent->children_[1]) {
            stack->push_back(&parent->children_[1]);
            if (parent->order_ > parent->children_[1]->order_) {
                return false;
            }
            if (!this->check_invariants(stack, count)) {
                return false;
            }
            stack->pop_back();
        }

#if BATT_DEBUG_PRIORITY_HEAP
        if (at_top && (*count != this->size_)) {
            std::cerr << "bad count!" << BATT_INSPECT(*count) << BATT_INSPECT(this->size_) << std::endl;
            this->dump(std::cerr);
        }
#endif  // BATT_DEBUG_PRIORITY_HEAP

        return !at_top || (*count == this->size_);
    }

    void dump(std::ostream& out, Item* parent = nullptr, int depth = 0)
    {
        if (!parent && depth == 0 && this->top_) {
            this->dump(out, this->top_);
            return;
        }

        out << std::string(depth * 2, ' ') << "- ";
        if (!parent) {
            out << "nullptr" << std::endl;
        } else {
            out << parent->order_ << std::endl;
            this->dump(out, parent->children_[0], depth + 1);
            this->dump(out, parent->children_[1], depth + 1);
        }
    }

   private:
    Item* top_ = nullptr;
    usize size_ = 0;
    Optional<IntT> min_value_;
};

}  //namespace detail

template <typename T>
class PriorityWatch
{
   public:
    using PriorityHandler = BasicAbstractHandler<detail::PriorityHeapItem<T>, StatusOr<T>>;

    /** \brief (INTERNAL USE ONLY) spin-lock bit - indicates the state variable is locked.
     */
    static constexpr u32 kLocked = 0x01;

    /** \brief (INTERNAL USE ONLY) indicates that the Watch is not closed.
     */
    static constexpr u32 kOpen = 0x02;

    /** \brief (INTERNAL USE ONLY) indicates that one or more handlers are attached to the Watch, awaiting
     * change notification.
     */
    static constexpr u32 kWaiting = 0x04;

    /** \brief (INTERNAL USE ONLY) indicates the Watch was closed with end-of-stream condition true
     */
    static constexpr u32 kClosedAtEnd = 0x08;

    /** \brief (INTERNAL USE ONLY) indicates the Watch was closed with end-of-stream condition false
     */
    static constexpr u32 kClosedBeforeEnd = 0x10;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    Status await_at_least(T min_value);

    T clamp_min_value(T min_value);

    T fetch_add(T delta);

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    Status get_final_status() const
    {
        constexpr u32 mask = PriorityWatch::kClosedAtEnd | PriorityWatch::kClosedBeforeEnd;

        switch (this->spin_state_.load() & mask) {
        case PriorityWatch::kClosedBeforeEnd:
            return Status{StatusCode::kClosedBeforeEndOfStream};

        case PriorityWatch::kClosedAtEnd:
            return Status{StatusCode::kEndOfStream};

        default:
            break;
        }

        return Status{StatusCode::kClosed};
    }

    u32 lock_observers() const
    {
        for (;;) {
            const u32 prior_state = this->spin_state_.fetch_or(kLocked);
            if ((prior_state & kLocked) == 0) {
                return prior_state;
            }
            std::this_thread::yield();
        }
    }

    void unlock_observers(u32 desired_state) const
    {
        this->spin_state_.store(desired_state & ~kLocked);
    }

    void notify(T new_value)
    {
        const auto post_change_state = this->spin_state_.load();
        if ((post_change_state & (kLocked | kWaiting)) == 0) {
            //
            // If there is a concurrent call to async_wait that results in a handler being added to the
            // `observers_` list, it must go through the following atomic events:
            //
            //  1. load value (phase 1), no change
            //  2. set kLocked
            //  3. load value (phase 2), no change
            //  4. set kWaiting
            //
            // The notifier thread (this call), when not waking observers, goes through the following atomic
            // events:
            //
            //  a. change value
            //  b. load spin state, observe not kLocked and not kWaiting
            //
            // (b) must occur before (1) [therefore (a) < (1)] or between (1) and (2) [(a) < (3)].  In either
            // case, the async_wait call will load the value *after* this thread changes it (a), so there will
            // be no spurious deadlocks.
            //
            return;
        }

        // Acquire the spinlock.
        //
        const auto pre_lock_state = this->lock_observers();
        HandlerList<StatusOr<T>> local_observers = std::move(this->observers_);
        this->unlock_observers(pre_lock_state & ~(kWaiting));

        invoke_all_handlers(&local_observers, new_value);
        //
        // IMPORTANT: we must not touch *anything* in `this` after invoking handlers, since one of the
        // handlers may delete this Watch object.
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    std::atomic<T> value_{0};
    mutable std::atomic<u32> spin_state_{/*kOpen*/ 0};
    mutable detail::PriorityHeap<T> observers_;
};

//#=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename IntT>
inline Status PriorityWatch<IntT>::await_at_least(IntT min_value)
{
    (void)min_value;
    return OkStatus();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename IntT>
inline IntT PriorityWatch<IntT>::clamp_min_value(IntT min_value)
{
    (void)min_value;
    return 0;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename IntT>
inline IntT PriorityWatch<IntT>::fetch_add(IntT delta)
{
    (void)delta;
    return 0;
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_PRIORITY_WATCH_HPP
