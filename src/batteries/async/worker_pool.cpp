//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/worker_pool.hpp>
#include <batteries/async/worker_pool_impl.hpp>

#endif  // !BATT_HEADER_ONLY
