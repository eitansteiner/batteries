//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/work_context.hpp>
#include <batteries/async/work_context_impl.hpp>

#endif  // !BATT_HEADER_ONLY
