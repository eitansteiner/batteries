//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_FUTURE_IPP
#define BATTERIES_ASYNC_FUTURE_IPP

#include <batteries/config.hpp>
//
#include <batteries/async/handler.hpp>
#include <batteries/async/task.hpp>
#include <batteries/shared_ptr.hpp>
#include <batteries/utility.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline Promise<T>::Promise() : impl_{batt::make_shared<detail::FutureImpl<T>>()}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline void Promise<T>::set_value(T&& value)
{
    this->impl_->set_value(BATT_FORWARD(value));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline void Promise<T>::set_value(StatusOr<T>&& value)
{
    this->impl_->set_value(std::move(value));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline void Promise<T>::set_value(const StatusOr<T>& value)
{
    this->impl_->set_value(value);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline void Promise<T>::set_error(const Status& status)
{
    this->impl_->set_error(status);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline Future<T>::Future(boost::intrusive_ptr<detail::FutureImpl<T>>&& impl) noexcept : impl_{std::move(impl)}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline Future<T> get_future(const Promise<T>& promise)
{
    return Future<T>{make_copy(promise.impl_)};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline bool Future<T>::is_ready() const
{
    return this->impl_->is_ready();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline StatusOr<T> Future<T>::await() const
{
    return ::batt::Task::await(*this);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
template <typename Handler>
inline void Future<T>::async_wait(Handler&& handler) const
{
    this->impl_->async_get(BATT_FORWARD(handler));
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_FUTURE_IPP
