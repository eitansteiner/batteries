#pragma once
#ifndef BATTERIES_ASYNC_BUFFER_SOURCE_IPP
#define BATTERIES_ASYNC_BUFFER_SOURCE_IPP

namespace batt {

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
//
class BufferSource::AbstractBufferSource : public AbstractValue<AbstractBufferSource>
{
   public:
    virtual usize size() const = 0;

    virtual StatusOr<SmallVec<ConstBuffer, 2>> fetch_at_least(i64 min_count) = 0;

    virtual void consume(i64 count) = 0;

    virtual void close_for_read() = 0;
};

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
//
template <typename T>
class BufferSource::BufferSourceImpl
    : public AbstractValueImpl<BufferSource::AbstractBufferSource, BufferSource::BufferSourceImpl, T>
{
   public:
    using AbstractValueImpl<BufferSource::AbstractBufferSource, BufferSource::BufferSourceImpl,
                            T>::AbstractValueImpl;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    // AbstractBufferSource interface

    usize size() const override
    {
        return unwrap_ref(this->obj_).size();
    }

    StatusOr<SmallVec<ConstBuffer, 2>> fetch_at_least(i64 min_count) override
    {
        return unwrap_ref(this->obj_).fetch_at_least(min_count);
    }

    void consume(i64 count) override
    {
        unwrap_ref(this->obj_).consume(count);
    }

    void close_for_read() override
    {
        unwrap_ref(this->obj_).close_for_read();
    }
};

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename /*= EnableIfNoShadow<BufferSource, T&&>*/,
          typename /*= EnableIfBufferSource<UnwrapRefType<T>>*/,
          typename /*= std::enable_if_t<std::is_same_v<std::decay_t<T>, T>>*/>
inline /*implicit*/ BufferSource::BufferSource(T&& obj) noexcept : impl_{StaticType<T>{}, BATT_FORWARD(obj)}
{
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_BUFFER_SOURCE_IPP
