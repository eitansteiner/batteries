//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_LATCH_IMPL_IPP
#define BATTERIES_ASYNC_LATCH_IMPL_IPP

#include <batteries/config.hpp>

#if BATT_HEADER_ONLY
#include <batteries/async/watch_impl.hpp>
#endif

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline bool Latch<T>::set_error(const batt::Status& status)
{
    BATT_CHECK(!status.ok()) << "Latch::set_error must be called with a non-ok Status value";

    return this->set_value(status);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
template <typename... Args>
inline bool Latch<T>::set_value(Args&&... args)
{
    const u32 prior_state = this->state_.fetch_or(kSetting);
    if (prior_state != kInitial) {
        return false;
    }
    this->value_.emplace(BATT_FORWARD(args)...);
    this->state_.set_value(kReady);
    return true;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline bool Latch<T>::is_ready() const
{
    return this->state_.get_value() == kReady;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline StatusOr<T> Latch<T>::await() & noexcept
{
    Status status = this->state_.await_equal(kReady);
    BATT_REQUIRE_OK(status);

    return this->get_ready_value_or_panic();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline StatusOr<T> Latch<T>::await() && noexcept
{
    Status status = this->state_.await_equal(kReady);
    BATT_REQUIRE_OK(status);

    return std::move(*this).get_ready_value_or_panic();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline StatusOr<T> Latch<T>::poll()
{
    if (this->state_.get_value() != kReady) {
        return Status{StatusCode::kUnavailable};
    }
    return this->get_ready_value_or_panic();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline StatusOr<T> Latch<T>::get_ready_value_or_panic() & noexcept
{
    BATT_CHECK_EQ(this->state_.get_value(), kReady);
    BATT_CHECK(this->value_);

    return *this->value_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline StatusOr<T> Latch<T>::get_ready_value_or_panic() && noexcept
{
    BATT_CHECK_EQ(this->state_.get_value(), kReady);
    BATT_CHECK(this->value_);

    return std::move(*this->value_);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
template <typename Handler>
inline void Latch<T>::async_get(Handler&& handler)
{
    this->state_.async_wait(kInitial, bind_handler(BATT_FORWARD(handler), AsyncGetHandler{this}));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline void Latch<T>::invalidate()
{
    this->state_.close();
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_LATCH_IMPL_IPP
