//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/async/buffered_channel.hpp>
//
#include <batteries/async/buffered_channel.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/asio/io_context.hpp>
#include <batteries/async/task.hpp>

namespace {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AsyncBufferedChannelTest, InitialStatusOk)
{
    batt::BufferedChannel<std::string> ch;
    EXPECT_EQ(ch.get_last_status(), batt::OkStatus());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AsyncBufferedChannelTest, NoWaitAsyncWriteWhenInactive)
{
    batt::BufferedChannel<std::string> ch;
    {
        batt::Status write_status = batt::Task::await<batt::Status>([&](auto&& handler) {
            ch.async_write(7, 'a', [handler = BATT_FORWARD(handler)](const batt::Status& status) mutable {
                BATT_FORWARD(handler)(status);
            });
        });
        EXPECT_EQ(write_status, batt::OkStatus());

        batt::Optional<std::string> read_data;
        {
            boost::asio::io_context io;
            batt::Task reader{io.get_executor(),
                              [&] {
                                  batt::StatusOr<std::string&> result = ch.read();
                                  BATT_CHECK_OK(result);

                                  read_data = *result;
                                  ch.consume();
                              },
                              "reader task"};

            io.run();
            reader.join();
        }

        ASSERT_TRUE(read_data);
        EXPECT_THAT(*read_data, ::testing::StrEq("aaaaaaa"));
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AsyncBufferedChannelTest, NoWaitWriteWhenInactive)
{
    batt::BufferedChannel<std::string> ch;
    {
        batt::Status write_status = ch.write(7, 'b');
        EXPECT_EQ(write_status, batt::OkStatus());

        batt::Optional<std::string> read_data;
        {
            boost::asio::io_context io;
            batt::Task reader{io.get_executor(),
                              [&] {
                                  batt::StatusOr<std::string&> result = ch.read();
                                  BATT_CHECK_OK(result);

                                  read_data = *result;
                                  ch.consume();
                              },
                              "reader task"};

            io.run();
            reader.join();
        }
        ASSERT_TRUE(read_data);
        EXPECT_THAT(*read_data, ::testing::StrEq("bbbbbbb"));
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AsyncBufferedChannelTestg, WaitOnSecondWrite)
{
    batt::BufferedChannel<std::string> ch;
    {
        batt::Status write_status = ch.write(7, 'c');
        EXPECT_EQ(write_status, batt::OkStatus());

        batt::Optional<std::string> read_data_1;
        batt::Optional<std::string> read_data_2;
        {
            boost::asio::io_context io;

            bool write_done = false;
            batt::Task writer{io.get_executor(),
                              [&] {
                                  batt::Status result = ch.write(3, 'd');
                                  BATT_CHECK_OK(result);
                                  write_done = true;
                              },
                              "writer task (2)"};

            io.poll();
            io.reset();

            EXPECT_FALSE(write_done);

            batt::Task reader_1{io.get_executor(),
                                [&] {
                                    batt::StatusOr<std::string&> result = ch.read();
                                    BATT_CHECK_OK(result);

                                    read_data_1 = *result;
                                    ch.consume();
                                },
                                "reader task (1)"};

            io.poll();
            io.reset();

            ASSERT_TRUE(read_data_1);
            EXPECT_THAT(*read_data_1, ::testing::StrEq("ccccccc"));
            EXPECT_FALSE(read_data_2);
            EXPECT_TRUE(write_done);

            writer.join();

            batt::Task reader_2{io.get_executor(),
                                [&] {
                                    batt::StatusOr<std::string&> result = ch.read();
                                    BATT_CHECK_OK(result);

                                    read_data_2 = *result;
                                    ch.consume();
                                },
                                "reader task (2)"};

            io.run();
            reader_2.join();
        }
        ASSERT_TRUE(read_data_2);
        EXPECT_THAT(*read_data_2, ::testing::StrEq("ddd"));
    }
}

}  // namespace
