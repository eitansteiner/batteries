//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/buffer_source.hpp>
//
#include <batteries/async/buffer_source_impl.hpp>

#endif  // !BATT_HEADER_ONLY
