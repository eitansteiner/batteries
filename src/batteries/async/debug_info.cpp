//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/debug_info.hpp>
//
#include <batteries/async/debug_info_impl.hpp>

#endif  // !BATT_HEADER_ONLY
