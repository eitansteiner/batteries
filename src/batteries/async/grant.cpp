//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/grant.hpp>
//
#include <batteries/async/grant_impl.hpp>

#endif  // !BATT_HEADER_ONLY
