//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi
//

#include <batteries/config.hpp>
//
#include <batteries/async/backoff_decl.hpp>

#include <batteries/async/backoff.ipp>

#if BATT_HEADER_ONLY
#include <batteries/async/backoff_impl.hpp>
#endif
