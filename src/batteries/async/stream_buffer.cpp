//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/stream_buffer.hpp>
//
#include <batteries/async/stream_buffer_impl.hpp>

#endif  // !BATT_HEADER_ONLY
