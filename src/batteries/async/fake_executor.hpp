// Copyright 2022-2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>
//
#include <batteries/async/fake_executor_decl.hpp>
//
#include <batteries/async/fake_executor.ipp>

#if BATT_HEADER_ONLY
#include <batteries/async/fake_executor_impl.hpp>
#endif
