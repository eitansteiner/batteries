//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_BUFFERED_CHANNEL_HPP
#define BATTERIES_ASYNC_BUFFERED_CHANNEL_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/channel.hpp>
#include <batteries/async/handler.hpp>

#include <batteries/utility.hpp>

#include <type_traits>

namespace batt {

template <typename T>
class BufferedChannel
{
   public:
    using value_type = T;

    BufferedChannel() = default;

    BufferedChannel(const BufferedChannel&) = delete;
    BufferedChannel& operator=(const BufferedChannel&) = delete;

    ~BufferedChannel() noexcept = default;

    Status get_last_status() const noexcept
    {
        return this->last_status_;
    }

    bool is_active() const
    {
        return this->channel_.is_active();
    }

    void await_inactive() noexcept
    {
        this->channel_.await_inactive();
    }

    template <typename Handler = void(StatusOr<T&>)>
    void async_read(Handler&& handler)
    {
        this->channel_.async_read(BATT_FORWARD(handler));
    }

    StatusOr<T&> read()
    {
        return this->channel_.read();
    }

    void consume()
    {
        this->channel_.consume();
    }

    void close_for_read()
    {
        this->channel_.close_for_read();
    }

    template <typename... ArgsAndHandler>
    void async_write(ArgsAndHandler&&... args_and_handler)
    {
        static_assert(sizeof...(ArgsAndHandler) > 0, "");

        // Rotate the args one place to the right, to bring the handler to the front of the arg list.
        //
        rotate_args_right(
            [this](auto&& handler, auto&&... args) {
                this->async_write_impl(BATT_FORWARD(handler), BATT_FORWARD(args)...);
            },
            BATT_FORWARD(args_and_handler)...);
    }

    template <typename... Args>
    Status write(Args&&... args)
    {
        this->channel_.await_inactive();
        return this->write_assuming_inactive(BATT_FORWARD(args)...);
    }

    void close_for_write()
    {
        this->channel_.close_for_write();
    }

   private:
    //+++++++++++-+-+--+----- --- -- -  -  -   -

    template <typename... Args>
    Status write_assuming_inactive(Args&&... args)
    {
        Status local_status;
        std::swap(this->last_status_, local_status);

        T& value = this->buffer_.emplace(BATT_FORWARD(args)...);

        this->channel_.async_write(value, [this](const Status& status) {
            this->last_status_ = status;
        });

        return local_status;
    }

    template <typename Handler, typename... Args>
    void async_write_impl(Handler&& handler, Args&&... args)
    {
        this->channel_.async_wait_inactive(
            bind_handler(BATT_FORWARD(handler),
                         [this, args_tuple = std::make_tuple(BATT_FORWARD(args)...)](auto&& handler) mutable {
                             Status status = std::apply(
                                 [this](auto&&... args) {
                                     return this->write_assuming_inactive(BATT_FORWARD(args)...);
                                 },
                                 std::move(args_tuple));

                             BATT_FORWARD(handler)(status);
                         }));
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    Channel<T> channel_;
    Optional<T> buffer_;
    Status last_status_;
};

}  // namespace batt

#endif  // BATTERIES_ASYNC_BUFFERED_CHANNEL_HPP
