//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_TASK_DECL_HPP
#define BATTERIES_ASYNC_TASK_DECL_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/async/continuation.hpp>
#include <batteries/async/future_decl.hpp>
#include <batteries/async/handler.hpp>
#include <batteries/async/io_result.hpp>
#include <batteries/async/watch_decl.hpp>
#include <batteries/case_of.hpp>
#include <batteries/constants.hpp>
#include <batteries/finally.hpp>
#include <batteries/int_types.hpp>
#include <batteries/logging.hpp>
#include <batteries/optional.hpp>
#include <batteries/segv.hpp>
#include <batteries/status.hpp>
#include <batteries/suppress.hpp>
#include <batteries/utility.hpp>

BATT_SUPPRESS_IF_GCC("-Wswitch-enum")
BATT_SUPPRESS_IF_GCC("-Wsuggest-override")
BATT_SUPPRESS_IF_CLANG("-Wswitch-enum")
BATT_SUPPRESS_IF_CLANG("-Wsuggest-override")
BATT_SUPPRESS_IF_CLANG("-Wunused-variable")
//
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/defer.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/executor.hpp>
#include <boost/asio/executor_work_guard.hpp>
#include <boost/asio/post.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/preprocessor/cat.hpp>
//
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_GCC()
BATT_UNSUPPRESS_IF_GCC()

#include <atomic>
#include <bitset>
#include <functional>
#include <future>
#include <thread>
#include <type_traits>
#include <utility>

namespace batt {

/** Returns the lowest unused global thread id number; repeated calls to `next_thread_id()` will return
 *  monotonically increasing values.
 */
i32 next_thread_id();

/** Returns a reference to the thread-local id for the current thread.
 */
i32& this_thread_id();

// Forward-declaration.
//
class DebugInfoFrame;

/** \brief A user-space cooperatively scheduled thread of control.
 *
 * Does not support preemption.
 */
class Task
    : public boost::intrusive::list_base_hook<boost::intrusive::link_mode<boost::intrusive::auto_unlink>>
{
    friend class DebugInfoFrame;

    friend void print_debug_info(DebugInfoFrame* p, std::ostream& out);

   public:
    /** \brief If a DeferStart{true} arg is passed to Task::Task(...), then the task must be started
     * explicitly via `start()`.
     */
    BATT_STRONG_TYPEDEF(bool, DeferStart);

    /** \brief Integer type representing the atomic state of a Task.
     */
    using state_type = u32;

    /** \brief A Task priority; Tasks with higher values are scheduled before those with lower values.
     */
    BATT_STRONG_TYPEDEF_WITH_DEFAULT(i32, Priority, 0);

    /** \brief Whether a task is done executing.
     */
    BATT_STRONG_TYPEDEF(bool, IsDone);

    /** \brief The executor for a Task; this type is responsible for running the Task via `boost::asio::post`
     * and/or `boost::asio::dispatch`.
     */
    using executor_type = boost::asio::any_io_executor;

    /** \brief The type of a global linked list of tasks maintained by the Batteries runtime.
     */
    using TaskList = boost::intrusive::list<Task, boost::intrusive::constant_time_size<false>>;

    /** \brief Thread-safe list of tasks.
     */
    class ConcurrentTaskList
    {
       public:
        ConcurrentTaskList() = default;

        ConcurrentTaskList(const ConcurrentTaskList&) = delete;
        ConcurrentTaskList& operator=(const ConcurrentTaskList&) = delete;

        void push_back(Task& task);

        void unlink(Task& task);

        void await_empty();

        template <typename Fn, typename R = std::invoke_result_t<Fn&&, TaskList&>>
        R with_lock(Fn&& fn);

       private:
        std::mutex mutex_;
        TaskList task_list_;
        std::atomic<i64> link_count_{0};
        Watch<i64> unlink_count_{0};
    };

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    // Task creation defaults
    //----- --- -- -  -  -   -

    /** \brief The default stack size.  Be generous and let the MMU take care of the rest...
     */
    static constexpr StackSize kDefaultStackSize = StackSize{8 * kMiB};

    /** \brief The default stack type.
     */
    static constexpr StackType kDefaultStackType = StackType::kFixedSize;

    /** \brief By default, tasks start immediately upon construction.
     */
    static constexpr DeferStart kDefaultDeferStart = DeferStart{false};

    /** \brief The name given to a \ref batt::Task if none is passed into the constructor.
     */
    static std::string default_name()
    {
        return "(anonymous)";
    }

    /** \brief Returns the default task priority (based on the current task priority).
     */
    static Priority default_priority()
    {
        return Priority{Task::current_priority() + 100};
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    /** \brief Optional params for Task creation.
     */
    struct Options {
        Optional<std::string> name;
        Optional<StackSize> stack_size;
        Optional<StackType> stack_type;
        Optional<Priority> priority;
        Optional<DeferStart> defer_start;

        //+++++++++++-+-+--+----- --- -- -  -  -   -

        template <typename... Args>
        static Options from_args(Args&&... args)
        {
            Options options;
            options.set_params(BATT_FORWARD(args)...);
            return options;
        }

        //+++++++++++-+-+--+----- --- -- -  -  -   -

        Options& set_params()
        {
            return *this;
        }

        template <typename... Args>
        Options& set_params(std::string&& name, Args&&... args)
        {
            this->name.emplace(std::move(name));
            return this->set_params(BATT_FORWARD(args)...);
        }

        template <typename... Args>
        Options& set_params(StackSize stack_size, Args&&... args)
        {
            this->stack_size = stack_size;
            return this->set_params(BATT_FORWARD(args)...);
        }

        template <typename... Args>
        Options& set_params(StackType stack_type, Args&&... args)
        {
            this->stack_type = stack_type;
            return this->set_params(BATT_FORWARD(args)...);
        }

        template <typename... Args>
        Options& set_params(Priority priority, Args&&... args)
        {
            this->priority = priority;
            return this->set_params(BATT_FORWARD(args)...);
        }

        template <typename... Args>
        Options& set_params(DeferStart defer_start, Args&&... args)
        {
            this->defer_start = defer_start;
            return this->set_params(BATT_FORWARD(args)...);
        }

        //+++++++++++-+-+--+----- --- -- -  -  -   -

        std::string get_name() && noexcept
        {
            if (this->name) {
                return std::move(*this->name);
            }
            return Task::default_name();
        }

        std::string get_name() const&& noexcept
        {
            if (this->name) {
                return *this->name;
            }
            return Task::default_name();
        }

        std::string get_name() const& noexcept
        {
            return std::move(*this).get_name();
        }

        StackSize get_stack_size() const noexcept
        {
            return this->stack_size.value_or(kDefaultStackSize);
        }

        StackType get_stack_type() const noexcept
        {
            return this->stack_type.value_or(kDefaultStackType);
        }

        Priority get_priority() const noexcept
        {
            if (this->priority) {
                return *this->priority;
            }
            return Task::default_priority();
        }

        DeferStart get_defer_start() const noexcept
        {
            return this->defer_start.value_or(kDefaultDeferStart);
        }
    };

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    /** Thread-local counter that limits stack growth while running Tasks via `dispatch`.
     */
    static usize& nesting_depth();

    /** \brief The upper bound on \ref nesting_depth.
     *
     * When scheduling a task to run via `dispatch` would increase the nesting depth on the current thread to
     * greater than \ref kMaxNestingDepth, `post` is used instead.
     */
    static constexpr usize kMaxNestingDepth = 8;

    /** The number of bytes to statically allocate for handler memory buffers.
     */
    static constexpr usize kHandlerMemoryBytes = 128;

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    /** Set when code within the task requests a signal, because it is awaiting some external async event.
     */
    static constexpr state_type kNeedSignal = state_type{1} << 0;

    /** Set when the continuation generated by an `await` is invoked.
     */
    static constexpr state_type kHaveSignal = state_type{1} << 1;

    /** Set when the task is not currently running.
     */
    static constexpr state_type kSuspended = state_type{1} << 2;

    /** Indicates the task has finished execution.
     */
    static constexpr state_type kTerminated = state_type{1} << 3;

    /** Set to request that the task collect a stack trace the next time it resumes.
     */
    static constexpr state_type kStackTrace = state_type{1} << 4;

    /** Spin-lock bit to serialize access to the sleep timer member of the Task.
     */
    static constexpr state_type kSleepTimerLock = state_type{1} << 5;

    /** Spin-lock bit to serialize access to the completions handlers list.
     */
    static constexpr state_type kCompletionHandlersLock = state_type{1} << 6;

    /** Used to save the value of the \ref kSleepTimerLock bit when the Task is suspended (e.g., in `await` or
     * `yield`).  The Task should not hold any spinlocks while it is suspended, so we don't deadlock.  Rather,
     * the sleep timer lock is temporarily released while suspended and then re-acquired when the task is
     * resumed.
     */
    static constexpr state_type kSleepTimerLockSuspend = state_type{1} << 7;

    /** State bit to indicate that completion handlers should not be added to the list, but called
     * immediately.
     */
    static constexpr state_type kCompletionHandlersClosed = state_type{1} << 8;

    /** \brief State bit that indicates all fields have been initialized.
     */
    static constexpr state_type kInitialized = state_type{1} << 9;

    /** \brief State bit that indicates all fields have been initialized.
     */
    static constexpr state_type kStarted = state_type{1} << 10;

    /** The number of state flags defined above.
     */
    static constexpr usize kNumStateFlags = 11;

    //----- --- -- -  -  -   -
    //
    static_assert((kNumStateFlags + 7) / 8 <= sizeof(state_type),
                  "state_type does not have enough bits to store all the state flags!");
    //
    //----- --- -- -  -  -   -

    /** The bitset type for a state.
     */
    using StateBitset = std::bitset<kNumStateFlags>;

    /** \brief Returns true iff the given state indicates the Task has been initialized.
     */
    static constexpr bool is_post_init_state(state_type state)
    {
        return (state & kInitialized) == kInitialized;
    }

    /** \brief Returns true iff the given state is initialized but not started.
     */
    static constexpr bool is_pre_start_state(state_type state)
    {
        // IMPORTANT: this must be an **exact** match.
        //
        return (state == (kInitialized | kNeedSignal));
    }

    /** \brief Returns true iff the given state is started.
     */
    static constexpr bool is_started_state(state_type state)
    {
        return (state & (kInitialized | kStarted)) == (kInitialized | kStarted);
    }

    /** Returns true iff the given state is *not* a suspended state.
     */
    static constexpr bool is_running_state(state_type state)
    {
        return (state & (kInitialized | kStarted | kSuspended)) == (kInitialized | kStarted);
    }

    /** Returns true iff the task is not currently running, but is ready to be resumed.
     */
    static constexpr bool is_ready_state(state_type state)
    {
        return
            // The task must be suspended but not terminated.
            //
            ((state & (kInitialized | kStarted | kSuspended | kTerminated)) ==
             (kInitialized | kStarted | kSuspended))

            && (  // *Either* task is not waiting for a signal...
                  //
                   (state & (kNeedSignal | kHaveSignal)) == 0 ||

                   // ...*Or* task was waiting for a signal, and it received one.
                   //
                   (state & (kNeedSignal | kHaveSignal)) == (kNeedSignal | kHaveSignal)  //

                   )

            // The stack trace flag is not set.
            //
            && ((state & kStackTrace) == 0);
    }

    /** Returns true if the passed state represents a fully terminated task.
     */
    static constexpr bool is_terminal_state(state_type state)
    {
        return (state & (kInitialized | kStarted | kSuspended | kTerminated)) ==
               (kInitialized | kStarted | kSuspended | kTerminated);
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    /** Stack trace and debug information collected from a Task.
     */
    struct DebugTrace {
        boost::stacktrace::stacktrace stack_trace;
        std::string debug_info;
        StateBitset state_bits;
        isize stack_growth_bytes;
    };

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Returns a reference to the global task list.
     */
    static ConcurrentTaskList& all_tasks();

    /** \brief Returns a reference to the currently running Task, if there is one.
     *
     * WARNING: if this method is called outside of any batt::Task, behavior is undefined.
     */
    static Task& current();

    /** \brief Launch a Task that is a child of the current one.  The new task will automatically be joined
     * when the current Task terminates.
     */
    template <typename... CtorArgs>
    static void spawn(CtorArgs&&... args)
    {
        Task* parent = Task::current_ptr();
        if (parent == nullptr) {
            Task::spawn_impl(Task::all_tasks(), BATT_FORWARD(args)...);
        } else {
            Task::spawn_impl(parent->child_tasks_, BATT_FORWARD(args)...);
        }
    }

    /** \brief Returns the current task name, or "" if there is no current task.
     *
     * Unlike batt::Task::current(), this method is safe to call outside a task.
     */
    static std::string_view current_name()
    {
        auto ptr = Task::current_ptr();
        if (ptr) {
            return ptr->name();
        }
        return "";
    }

    /** \brief Returns the unique id number of the current Task or thread.
     */
    static i32 current_id()
    {
        auto ptr = Task::current_ptr();
        if (ptr) {
            return ptr->id();
        }
        thread_local const i32 id_ = Task::next_id();
        return id_;
    }

    /** \brief Returns the current stack position, if currently inside a task.
     *
     * \return If called from inside a task, the current stack position in bytes, else \ref batt::None
     */
    static Optional<usize> current_stack_pos();

    /** brief Returns the stack position of `ptr` relative to the current stack base, if currently inside a
     * task.
     *
     * NOTE: If `ptr` isn't actually on the current task's stack, then this function will still return a
     * number, but it will be essentially a garbage value.  It's up to the caller to make sure that `ptr`
     * points at something on the task stack.
     *
     * \return  If called from inside a task, the stack offset in bytes of `ptr`, else \ref batt::None
     */
    static Optional<usize> current_stack_pos_of(const volatile void* ptr);

    /** \brief Dumps stack traces and debug info from all Tasks and threads to stderr.
     *
     * \param force If true, then this function will attempt to dump debug information for running tasks, even
     *              though this may cause data races (if you're debugging a tricky threading issue, sometimes
     *              the risk of a crash is outweighed by the benefit of some additional clues about what's
     *              going on!)
     *
     * \return The number of tasks dumped.
     */
    static i32 backtrace_all(bool force, std::ostream& out = std::cerr);

    /** \brief Yields control from the current Task/thread, allowing other tasks to run.
     *
     * Suspends the current task and immediately schedules it to resume via `boost::asio::post`.
     */
    static void yield();

    /** \brief Puts the current Task/thread to sleep for the specified duration.
     *
     * This operation can be interrupted by a \ref batt::Task::wake(), in which case a "cancelled"
     * error code is returned instead of success (no error).
     *
     * This method is safe to call outside a task; in this case, it is implemented via
     * `std::this_task::sleep_for`.
     *
     * \return `batt::ErrorCode{}` (no error) if the specified duration passed, else
     * `boost::asio::error::operation_aborted` (indicating that \ref batt::Task::wake() was called on the
     * given task)
     */
    template <typename Duration = boost::posix_time::ptime>
    static ErrorCode sleep(const Duration& duration)
    {
        Task* current_task = Task::current_ptr();
        if (current_task) {
            return current_task->sleep_impl(duration);
        }

        std::this_thread::sleep_for(std::chrono::nanoseconds(duration.total_nanoseconds()));
        return ErrorCode{};
    }

    /** \brief Suspends the current thread/Task until an asynchronous event occurs.
     *
     *  The param `fn` is passed a continuation handler that will cause this Task to wake up, causing \ref
     * await to return an instance of type `R` constructed from the arguments passed to the handler.  For
     * example, `await` can be used to turn an async socket read into a synchronous call:
     *
     * ```
     * boost::asio::ip::tcp::socket s;
     *
     * using ReadResult = std::pair<boost::system::error_code, std::size_t>;
     *
     * ReadResult r = Task::await<ReadResult>([&](auto&& handler) {
     *     s.async_read_some(buffers, BATT_FORWARD(handler));
     *   });
     *
     * if (r.first) {
     *   std::cout << "Error! ec=" << r.first;
     * } else {
     *   std::cout << r.second << " bytes were read.";
     * }
     * ```
     */
    template <typename R, typename Fn>
    static R await(Fn&& fn)
    {
        // If there is a Task active on the current thread, use the Task impl of await.
        //
        Task* current_task = Task::current_ptr();
        if (current_task) {
            return current_task->template await_impl<R>(BATT_FORWARD(fn));
        }

        //---------------
        // This is the generic thread (non-Task) implementation:
        //
        HandlerMemory<kHandlerMemoryBytes> handler_memory;
        std::promise<R> prom;
        std::atomic<bool> ok_to_exit{false};
        BATT_FORWARD(fn)
        (make_custom_alloc_handler(handler_memory, [&prom, &ok_to_exit](auto&&... args) {
            prom.set_value(R{BATT_FORWARD(args)...});
            ok_to_exit.store(true);
        }));

        auto wait_for_promise = batt::finally([&] {
            while (!ok_to_exit.load()) {
                std::this_thread::yield();
            }
        });

        // TODO [tastolfi 2020-12-01] - detect deadlock here

        return prom.get_future().get();
    }

    // TODO [tastolfi 2021-12-22] - Implement await_with_timeout

    /** \brief Suspends the current thread/Task until an asynchronous event occurs.
     *
     * This overload takes the return type as an explicit formal parameter (instead of a template
     * parameter).
     */
    template <typename R, typename Fn>
    static R await(batt::StaticType<R>, Fn&& fn)
    {
        return Task::await<R>(BATT_FORWARD(fn));
    }

    /** \brief Suspends the current thread/Task until the passed Future is ready.
     */
    template <typename T>
    static StatusOr<T> await(const Future<T>& future_result)
    {
        return Task::await<StatusOr<T>>([&](auto&& handler) {
            future_result.async_wait(BATT_FORWARD(handler));
        });
    }

    /** \brief Convenience function that calls `async_read_some` on the passed stream and awaits the result.
     */
    template <typename AsyncStream, typename BufferSequence>
    static IOResult<usize> await_read_some(AsyncStream& s, BufferSequence&& buffers)
    {
        return Task::await<IOResult<usize>>([&](auto&& handler) {
            s.async_read_some(BATT_FORWARD(buffers), BATT_FORWARD(handler));
        });
    }

    /** \brief Convenience function that calls `async_read` on the passed stream and awaits the result.
     */
    template <typename AsyncStream, typename BufferSequence>
    static IOResult<usize> await_read(AsyncStream& s, BufferSequence&& buffers)
    {
        return Task::await<IOResult<usize>>([&](auto&& handler) {
            boost::asio::async_read(s, BATT_FORWARD(buffers), BATT_FORWARD(handler));
        });
    }

    /** \brief Convenience function that calls `async_write_some` on the passed stream and awaits the result.
     */
    template <typename AsyncStream, typename BufferSequence>
    static IOResult<usize> await_write_some(AsyncStream& s, BufferSequence&& buffers)
    {
        return Task::await<IOResult<usize>>([&](auto&& handler) {
            s.async_write_some(BATT_FORWARD(buffers), BATT_FORWARD(handler));
        });
    }

    /** \brief Convenience function that calls `async_write` on the passed stream and awaits the result.
     */
    template <typename AsyncStream, typename BufferSequence>
    static IOResult<usize> await_write(AsyncStream& s, BufferSequence&& buffers)
    {
        return Task::await<IOResult<usize>>([&](auto&& handler) {
            boost::asio::async_write(s, BATT_FORWARD(buffers), BATT_FORWARD(handler));
        });
    }

    /** \brief Convenience function that calls `async_connect` on the passed stream and awaits the result.
     */
    template <typename AsyncStream, typename Endpoint>
    static ErrorCode await_connect(AsyncStream& s, const Endpoint& endpoint)
    {
        return Task::await<ErrorCode>([&](auto&& handler) {
            s.async_connect(BATT_FORWARD(endpoint), BATT_FORWARD(handler));
        });
    }

    /** \brief Convenience function that calls `async_accept` on the passed stream and awaits the result.
     */
    template <typename AsyncAcceptor,                                      //
              typename ProtocolT = typename AsyncAcceptor::protocol_type,  //
              typename StreamT = typename ProtocolT::socket>
    static IOResult<StreamT> await_accept(AsyncAcceptor& a)
    {
        return Task::await<IOResult<StreamT>>([&](auto&& handler) {
            a.async_accept(BATT_FORWARD(handler));
        });
    }

    /** \brief Convenience function that calls `async_wait` on the passed signal_set and awaits the result.
     */
    template <typename SignalSetT>
    static IOResult<int> await_signal(SignalSetT& signal_set)
    {
        return Task::await<IOResult<int>>([&signal_set](auto&& handler) {
            signal_set.async_wait(BATT_FORWARD(handler));
        });
    }

    /** \brief Returns the priority of the current Task (or the default priority if no Task is active).
     *
     * NOTE: this function is safe to call outside of a task; in this case, the default priority (0) is
     * returned.
     */
    static Priority current_priority()
    {
        Task* current_task = Task::current_ptr();
        if (current_task == nullptr) {
            return Priority{0};
        }
        return current_task->get_priority();
    }

    /** \brief Returns true iff the current Task/thread is inside a Worker's work function.  Used to avoid
     * double-wait deadlocks on the same thread inside parallel algorithms.
     */
    static bool& inside_work_fn()
    {
        auto ptr = Task::current_ptr();
        if (ptr) {
            return ptr->is_inside_work_fn_;
        }

        thread_local bool b_ = false;
        return b_;
    }

    static std::atomic<i64>& create_count()
    {
        static std::atomic<i64> count_{0};
        return count_;
    }

    static std::atomic<i64>& destroy_count()
    {
        static std::atomic<i64> count_{0};
        return count_;
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Task is not copy-constructible.
     */
    Task(const Task&) = delete;

    /** \brief Task is not copy-assignable.
     */
    Task& operator=(const Task&) = delete;

    /** \brief Creates a new Task with a custom stack size.
     * DEPRECATED - pass stack_size *after* body_fn.
     */
    template <typename BodyFn = void()>
    explicit Task(const boost::asio::any_io_executor& ex, StackSize stack_size, BodyFn&& body_fn) noexcept
        : Task{ex, BATT_FORWARD(body_fn), /*name=*/default_name(), stack_size}
    {
    }

    /** \brief Create a new Task, optionally setting name, stack size, stack type, and priority.
     *
     * The default priority for a Task is the current task priority plus 100; this means that a new Task by
     * default will always "soft-preempt" the currently running task.
     */
    template <typename BodyFn = void(), typename... Args, typename = EnableIfNoShadow<Options, Args...>>
    explicit Task(const boost::asio::any_io_executor& ex, BodyFn&& body_fn, Args&&... args) noexcept
        : Task{Task::all_tasks(), ex, BATT_FORWARD(body_fn), BATT_FORWARD(args)...}
    {
    }

    /** \brief Create a new Task, with default options.
     */
    template <typename BodyFn = void()>
    explicit Task(const boost::asio::any_io_executor& ex, BodyFn&& body_fn) noexcept
        : Task{Task::all_tasks(), ex, BATT_FORWARD(body_fn), Options{}}
    {
    }

    /** \brief Internal use only.
     *
     * Supported Args... types:
     *     - std::string&& name
     *     - StackSize
     *     - StackType
     *     - Priority
     *     - DeferStart
     */
    template <typename BodyFn = void(), typename... Args, typename = EnableIfNoShadow<Options, Args...>>
    explicit Task(ConcurrentTaskList& parent_task_list, const boost::asio::any_io_executor& ex,
                  BodyFn&& body_fn, Args&&... args) noexcept
        : Task{parent_task_list, ex, BATT_FORWARD(body_fn), Options::from_args(BATT_FORWARD(args)...)}
    {
    }

    template <typename BodyFn = void()>
    explicit Task(ConcurrentTaskList& parent_task_list, const boost::asio::any_io_executor& ex,
                  BodyFn&& body_fn) noexcept
        : Task{parent_task_list, ex, BATT_FORWARD(body_fn), Options::from_args()}
    {
    }

    /** \brief Internal use only.
     */
    template <typename BodyFn = void(), typename OptionsT,
              typename = std::enable_if_t<std::is_same_v<std::decay_t<OptionsT>, Options>>>
    explicit Task(ConcurrentTaskList& parent_task_list, const boost::asio::any_io_executor& ex,
                  BodyFn&& body_fn, OptionsT&& options) noexcept
        : name_(std::move(options).get_name())
        , ex_(ex)
        , priority_{options.get_priority()}
        , parent_task_list_{parent_task_list}
    {
        // Initialize the task stack and run pre-body fn setup in the task context.  `pre_body_fn_entry` will
        // switch back to this context because kHaveSignal is set but kSignal is not.
        //
        this->self_ = callcc(  //
            options.get_stack_size(), options.get_stack_type(),
            [body_fn = ::batt::make_optional(BATT_FORWARD(body_fn)), this](Continuation&& parent) mutable {
                auto work_guard = boost::asio::make_work_guard(this->ex_);

                this->pre_body_fn_entry(std::move(parent));

                try {
                    (*body_fn)();
                } catch (...) {
                    BATT_LOG_WARNING() << "task fn exited via unhandled exception [task='" << this->name_
                                       << "']: " << boost::current_exception_diagnostic_information();
                }
                body_fn = None;

                return this->post_body_fn_exit();
            });

        // Add this task to the parent list.  If this is a top-level task (no parent), then `parent_task_list`
        // will be Task::all_tasks().
        //
        this->parent_task_list_.push_back(*this);

        // Set the initalized state bit, for sanity checking.
        //
        this->state_.fetch_or(kInitialized);

        // Unless defer start is true, start the task now (by setting the kStarted and kSignal bits to put the
        // task into a runnable state).
        //
        if (!options.get_defer_start()) {
            this->start();
        }

        // Track task creation/destruction (for diagnostics).
        //
        Task::create_count().fetch_add(1);
    }

    /** \brief Destroys the Task.
     *
     * A Task must be terminated when it is destroyed, or the program will panic.  Calling \ref Task::join()
     * prior to destroying a Task object is sufficient.
     */
    ~Task() noexcept;

    /** \brief Returns true iff this task has been started.
     */
    bool is_started() const
    {
        return Task::is_started_state(this->state_.load());
    }

    /** \brief Starts the task if not already started.
     */
    void start()
    {
        // Even if kStarted is already set, this is harmless.
        //
        const state_type prior_state = this->state_.fetch_or(kStarted);

        // If somehow we got here without having set the kInitialized bit, something has gone very wrong...
        //
        BATT_CHECK(Task::is_post_init_state(prior_state))
            << "Task started before being fully initialized!" << BATT_INSPECT(this->name())
            << BATT_INSPECT(this->id());

        // This should only trigger once.
        //
        if (Task::is_pre_start_state(prior_state)) {
            this->handle_event(kSuspended | kHaveSignal);
        }
    }

    /** \brief The unique id number assigned to this Task.
     */
    i32 id() const
    {
        return this->id_;
    }

    /** \brief The user-friendly name assigned to this Task.
     */
    std::string_view name() const
    {
        return this->name_;
    }

    /** \brief The scheduling priority of this task; higher == more urgent.
     */
    Priority get_priority() const
    {
        return Priority{this->priority_.load()};
    }

    /** \brief Assigns a new priority to this Task.  This method will not trigger a yield or activation; it
     * only affects future scheduling decisions.
     */
    void set_priority(Priority new_priority)
    {
        this->priority_.store(new_priority);
    }

    /** \brief The current byte offset of the top of this Task's stack.
     *
     * This value is only meaningful if this method is called while on the current task.  Usually you should
     * just call \ref batt::Task::current_stack_pos() instead.
     */
    usize stack_pos() const;

    /** \brief The byte offset of the given pointer relative to the base of this Task's stack; return value is
     * undefined if `ptr` is not on the stack of this Task!
     */
    usize stack_pos_of(const volatile void* ptr) const;

    /** \brief Blocks the current Task/thread until this Task has finished.
     */
    void join();

    /** \brief Returns whether or not this Task is finished. Equivalent to \ref is_done().
     *
     * This function is guaranteed never to block.
     */
    IsDone try_join();

    /** \brief Interrupts a call to \ref sleep on this Task.  Has no effect if the Task is not inside \ref
     * sleep.
     */
    bool wake();

    /** \brief The executor passed in to this Task at construction time.
     */
    executor_type get_executor() const
    {
        return this->ex_;
    }

    /** \brief Returns whether or not this Task is finished.  Equivalent to \ref try_join().
     */
    IsDone is_done() const;

    /** \brief Attaches a listener callback to the task; this callback will be invoked when the task completes
     * execution.
     *
     * This method can be thought of as an asynchronous version of \ref batt::Task::join.
     *
     * \param handler The handler to invoke when the Task has finished; should have the signature
                      `#!cpp void()`
     */
    template <typename F = void()>
    void call_when_done(F&& handler)
    {
        for (;;) {
            if (this->is_done()) {
                this->wait_for_run_completion_handlers();
                BATT_FORWARD(handler)();
                return;
            }

            SpinLockGuard lock{this, kCompletionHandlersLock};
            if (Task::is_terminal_state(lock.prior_state()) ||
                (lock.prior_state() & kCompletionHandlersClosed) != 0) {
                // It's possible that the completion handlers list was cleared out after the call to
                // `is_done()` above, but before we grab the spin lock.  If so, keep retrying until we resolve
                // the race.
                //
                continue;
            }
            push_handler(&this->completion_handlers_, BATT_FORWARD(handler));
            return;
        }
    }

    // =#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

    DebugInfoFrame* debug_info = nullptr;

    // =#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

   private:
    //=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
    //
    class SpinLockGuard
    {
       public:
        explicit SpinLockGuard(Task* task, state_type mask) noexcept : task_{task}, mask_{mask}
        {
            this->prior_state_ = task_->spin_lock(mask);
        }

        SpinLockGuard(const SpinLockGuard&) = delete;
        SpinLockGuard& operator=(const SpinLockGuard&) = delete;

        ~SpinLockGuard() noexcept
        {
            task_->spin_unlock(mask_);
        }

        state_type prior_state() const
        {
            return this->prior_state_;
        }

       private:
        Task* const task_;
        const state_type mask_;
        state_type prior_state_;
    };

    class Trampoline
    {
       public:
        static void activate_task(Task* t);

        static Task* get_current_task();

       private:
        static Trampoline& per_thread_instance();

        Task* next_to_run_ = nullptr;
        Task* current_task_ = nullptr;
    };

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    static i32 next_id();

    static Task* current_ptr();

    /** \brief Launch a Task that is a child of this one.  The new task will automatically be joined when this
     * Task terminates.
     */
    template <typename... TaskCtorArgs>
    static void spawn_impl(ConcurrentTaskList& parent_task_list, TaskCtorArgs&&... task_ctor_args)
    {
        Task* const child = new Task{parent_task_list, BATT_FORWARD(task_ctor_args)...};

        child->call_when_done([child] {
            delete child;
        });
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    // Invoked in the task's context prior to entering the task function; yields control back to the parent
    // context, ensuring that the task function is invoked via the executor.
    //
    void pre_body_fn_entry(Continuation&& parent) noexcept;

    // Invoked in the task's context after the task function returns.
    //
    Continuation post_body_fn_exit() noexcept;

    // Suspend the task, resuming the parent context.
    //
    void yield_impl();

    // Set the timer to expire after the given duration, suspending the task in a manner identical to
    // `await_impl`.
    //
    ErrorCode sleep_impl(const boost::posix_time::time_duration& duration);

    // Clears state flags kSuspended|kNeedSignal|kHaveSignal and resumes the task via its executor.  If
    // `force_post` is true, the resume is always scheduled via boost::asio::post.  Otherwise, if
    // Task::nesting_depth() is below the limit, boost::asio::dispatch is used instead.  `observed_state` is
    // the last observed value of `Task::state_`.
    //
    void schedule_to_run(state_type observed_state, bool force_post = false);

    // Resumes execution of the task on the current thread; this is the normal code path, when the task
    // receives a signal or is ready to run.  Stack traces collected on the task do not use this method;
    // rather they directly call resume_impl after atomically setting the kStackTrace bit (conditional on the
    // thread *not* being in a running, ready-to-run, or terminal state).
    //
    IsDone run();

    // Switch the current thread context to the task and resume execution.
    //
    void resume_impl();

    // `fn` is passed a callable acting as the continutation of the suspended Task.  This continuation may
    // receive any set of arguments from which the await operation's result type `R` can be constructed.
    //
    template <typename R, typename Fn>
    R await_impl(Fn&& fn)
    {
        Optional<R> result;

        HandlerMemory<kHandlerMemoryBytes> handler_memory;

        const state_type prior_state = this->state_.fetch_or(kNeedSignal);
        BATT_CHECK_NE((prior_state & kHaveSignal), kHaveSignal) << "prior_state=" << StateBitset{prior_state};

        BATT_FORWARD(fn)
        (/*callback handler=*/make_custom_alloc_handler(
            handler_memory,
            [this,
             &result](auto&&... args) -> std::enable_if_t<std::is_constructible_v<R, decltype(args)&&...>> {
                result.emplace(BATT_FORWARD(args)...);

                this->handle_event(kHaveSignal);
            }));

        // Suspend this Task.  It will not be in a ready state until the kHaveSignal event has been handled.
        //
        this->yield_impl();

        return std::move(*result);
    }

    // Tells the task to handle events which may affect its running/suspended state.  This function is safe to
    // invoke inside the task or outside.  `event_mask` *must* be one of:
    //
    // - kHaveSignal
    // - kSuspended
    // - kTerminated
    //
    void handle_event(state_type event_mask);

    // Acquire a spin lock on the given state bit mask.  `lock_mask` must be one of:
    //
    // - kSleepTimerLock
    // - kCompletionHandlersLock
    //
    // Locks acquired via this function are not recursive.
    //
    state_type spin_lock(state_type lock_mask);

    // Same as `spin_lock`, except only try once to acquire the lock.  Returns `true` iff the lock was
    // acquired. Sets `prior_state` equal to the last observed value of `state_`.
    //
    bool try_spin_lock(state_type lock_mask, state_type& prior_state);

    // Release the given spin lock bit.  `lock_mask` must be a legal value passed to
    // `spin_lock`/`try_spin_lock`, and the calling thread must currently hold a lock on the given bit
    // (acquired via `spin_lock`/`try_spin_lock`).
    //
    void spin_unlock(state_type lock_mask);

    // Attempt to collect a stack trace from the task, dumping it to stderr if successful.  This will fail if
    // the task is running, ready-to-run, or terminated.  Returns true iff successful.
    //
    bool try_dump_stack_trace(bool force, std::ostream& out = std::cerr);

    // Activate this task via boost::asio::post.
    //
    void activate_via_post();

    // Activate this task via boost::asio::dispatch.
    //
    void activate_via_dispatch();

    // Create an activation completion handler for use inside `activate_via_post`, `activate_via_dispatch`,
    // etc.
    //
    auto make_activation_handler(bool via_post);

    // Joins to all child tasks.
    //
    void join_child_tasks();

    // Unconditionally removes completion handlers from `this` and runs them on the current thread/task.
    //
    void run_completion_handlers();

    // Waits for run_completion_handlers to run; must only be called *after* this->is_done() returns true.
    //
    void wait_for_run_completion_handlers();

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    // Process-unique serial id number for this Task; automatically assigned at construction time.
    //
    const i32 id_ = next_id();

    // Human-readable (non-unique) name for this Task; passed in at construction time.
    //
    const std::string name_;

    // The (Boost.Async) Executor used to activate/schedule this task.
    //
    executor_type ex_;

    // The most recent context from which this Task was activated/scheduled.  If this is non-empty, then the
    // task is active/running.  At most one of `scheduler_` and `self_` are non-empty at any given time.
    //
    Continuation scheduler_;

    // The current (suspended) context of this Task.  If this is non-copy, then the task is suspended/waiting.
    // At most one of `scheduler_` and `self_` are non-empty at any given time.
    //
    Continuation self_;

    // Contains all spin lock bits and run-state information for this task.  Initially set to `kNeedSignal`
    // because the task must receive the "go" signal before it can enter normal operation.
    //
    std::atomic<state_type> state_{kNeedSignal};

    // The current advisory priority for this task.  Higher numeric values signify more urgent priority.
    //
    std::atomic<Priority::value_type> priority_;

    // Used to implement this->sleep().
    //
    Optional<boost::asio::deadline_timer> sleep_timer_;

    // When a stack trace is gathered from a task, we must temporarily swap its context in, then capture the
    // stack, then swap back; this member stores the stack trace.  TODO [tastolfi 2023-02-21] there could be a
    // race condition on this field if two threads attempt to capture a stack trace on the same task
    // concurrently!
    //
    Optional<boost::stacktrace::stacktrace> stack_trace_;

    // A linked list of handlers that are waiting for notification when this task finishes/joins.
    //
    HandlerList<> completion_handlers_;

    // A handler-attached memory buffer used whenever this task is scheduled via dispatch or post;
    // conceptually, this memory is used to store linked-list pointers for the task on some scheduler queue.
    //
    HandlerMemory<kHandlerMemoryBytes> activate_memory_;

    // Set to an address near the base of the stack when the task begins running; used to estimate the current
    // stack growth.
    //
    const volatile u8* stack_base_ = nullptr;

    // Set to true when this task performs some action that causes a task with higher priority to move into a
    // ready state; this causes the current task to be rescheduled via `post()` so that the higher priority
    // task can be scheduled immediately, possibly on the same thread, via `dispatch()`.
    //
    bool is_preempted_ = false;

    // Used to prevent deadlock in WorkerPools.
    //
    bool is_inside_work_fn_ = false;

    // Monotonic counter, incremented whenever this task is suspended.
    //
    volatile usize suspend_count_ = 0;

    // Monotonic counter, incremented whenever this task is resumed.
    //
    volatile usize resume_count_ = 0;

    // A list of spawned tasks; will be joined when this terminates.
    //
    ConcurrentTaskList child_tasks_;

    // The list to which this task was added at creation time.
    //
    ConcurrentTaskList& parent_task_list_;
};

//#=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Fn, typename R>
R Task::ConcurrentTaskList::with_lock(Fn&& fn)
{
    std::unique_lock<std::mutex> lock{this->mutex_};
    return BATT_FORWARD(fn)(this->task_list_);
}

}  // namespace batt

#endif  // BATTERIES_ASYNC_TASK_HPP
