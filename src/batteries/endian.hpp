//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ENDIAN_HPP
#define BATTERIES_ENDIAN_HPP

#include <batteries/config.hpp>
//
#include <batteries/int_types.hpp>

#include <boost/endian/conversion.hpp>
#include <boost/preprocessor/cat.hpp>

#include <type_traits>

namespace batt {

#define BATT_DEFINE_BIG_ENDIAN_LESS_THAN(bits)                                                               \
    inline bool big_endian_less_than(BOOST_PP_CAT(u, bits) a, BOOST_PP_CAT(u, bits) b)                       \
    {                                                                                                        \
        if (boost::endian::order::little == boost::endian::order::native) {                                  \
            return BOOST_PP_CAT(__builtin_bswap, bits)(a) < BOOST_PP_CAT(__builtin_bswap, bits)(b);          \
        }                                                                                                    \
        return a < b;                                                                                        \
    }

BATT_DEFINE_BIG_ENDIAN_LESS_THAN(16)
BATT_DEFINE_BIG_ENDIAN_LESS_THAN(32)
BATT_DEFINE_BIG_ENDIAN_LESS_THAN(64)

#undef BATT_DEFINE_BIG_ENDIAN_LESS_THAN

}  //namespace batt

#endif  // BATTERIES_ENDIAN_HPP
