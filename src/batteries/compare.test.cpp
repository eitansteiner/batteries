#include <batteries/compare.hpp>
//
#include <batteries/compare.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/stream_util.hpp>

#include <chrono>
#include <fstream>
#include <string>
#include <vector>

namespace {

using namespace batt::int_types;

std::vector<std::string> load_words()
{
    std::vector<std::string> words;
    std::ifstream ifs{"/usr/share/dict/words"};
    std::string word;
    while (ifs.good()) {
        ifs >> word;
        words.emplace_back(word);
    }
    return words;
}

batt::Order slow_compare(const std::string_view& a, const std::string_view& b) noexcept
{
    if (a < b) {
        return batt::Order::Less;
    }
    if (b < a) {
        return batt::Order::Greater;
    }
    return batt::Order::Equal;
}

TEST(Compare, Test)
{
    std::vector<std::string> words = load_words();
    const usize count = std::min<usize>(words.size(), 10000);

    std::cerr << BATT_INSPECT(count) << std::endl;

    std::vector<batt::Order> expected;
    std::vector<batt::Order> actual;

    expected.reserve(count * count);
    actual.reserve(count * count);

    {
        const auto start = std::chrono::steady_clock::now();
        for (usize i = 0; i < count; ++i) {
            for (usize j = 0; j < count; ++j) {
                expected.emplace_back(slow_compare(words[i], words[j]));
            }
        }
        std::cerr << "slow compare took "
                  << double(std::chrono::duration_cast<std::chrono::microseconds>(
                                std::chrono::steady_clock::now() - start)
                                .count()) /
                         1000000.0
                  << " seconds" << std::endl;
    }
    {
        const auto start = std::chrono::steady_clock::now();
        for (usize i = 0; i < count; ++i) {
            for (usize j = 0; j < count; ++j) {
                actual.emplace_back(batt::compare(words[i], words[j]));
            }
        }
        std::cerr << "batt::compare took "
                  << double(std::chrono::duration_cast<std::chrono::microseconds>(
                                std::chrono::steady_clock::now() - start)
                                .count()) /
                         1000000.0
                  << " seconds" << std::endl;
    }

    EXPECT_EQ(expected, actual);
}

}  // namespace
