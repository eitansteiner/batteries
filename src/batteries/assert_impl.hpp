#pragma once
#ifndef BATTERIES_ASSERT_IMPL_HPP
#define BATTERIES_ASSERT_IMPL_HPP

#include <mutex>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_NORETURN BATT_INLINE_IMPL void fail_check_exit()
{
    BATT_FAIL_CHECK_OUT << std::endl << std::endl;
    std::abort();
    BATT_UNREACHABLE();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool lock_fail_check_mutex()
{
    static std::aligned_storage_t<sizeof(std::mutex), alignof(std::mutex)> storage_;
    static std::mutex* m = new (&storage_) std::mutex{};
    thread_local bool locked = false;
    if (!locked) {
        m->lock();
        locked = true;
    }
    return true;
}

}  //namespace batt

#endif  // BATTERIES_ASSERT_IMPL_HPP
