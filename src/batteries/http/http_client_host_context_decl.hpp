//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_CLIENT_HOST_CONTEXT_DECL_HPP
#define BATTERIES_HTTP_HTTP_CLIENT_HOST_CONTEXT_DECL_HPP

#include <batteries/config.hpp>
//

#include <batteries/http/host_address.hpp>
#include <batteries/http/http_client_connection_decl.hpp>

#include <batteries/async/queue.hpp>
#include <batteries/async/task.hpp>
#include <batteries/async/watch.hpp>

#include <batteries/int_types.hpp>
#include <batteries/logging.hpp>
#include <batteries/optional.hpp>
#include <batteries/shared_ptr.hpp>
#include <batteries/small_vec.hpp>

#include <tuple>

namespace batt {

class HttpClient;
class HttpRequest;
class HttpResponse;

class HttpClientHostContext : public RefCounted<HttpClientHostContext>
{
   public:
    static constexpr usize kDefaultMaxConnections = 2;

    static constexpr usize kDefaultIdleConnectionTimeoutMs = 60 * 1000;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    explicit HttpClientHostContext(HttpClient& client, const HostAddress& host_address);

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    boost::asio::io_context& get_io_context();

    HttpClient& client() const noexcept;

    void set_max_connections(usize n) noexcept
    {
        this->max_connections_.store(n);
    }

    void set_connection_timeout_ms(i32 timeout_ms) noexcept
    {
        this->connection_timeout_ms_.store(timeout_ms);
    }

    i32 get_connection_timeout_ms() const noexcept
    {
        return this->connection_timeout_ms_.load();
    }

    usize count_active_connections() const noexcept
    {
        return this->active_connections_.get_value();
    }

    Status submit_request(Pin<HttpRequest>&& request, Pin<HttpResponse>&& response);

    void halt();

    void join();

    bool can_grow() const;

    const HostAddress& host_address() const;

    StatusOr<std::tuple<Pin<HttpRequest>, Pin<HttpResponse>>> await_next_request();

   private:
    void host_task_main();

    void create_connection();

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    HttpClient& client_;

    HostAddress host_address_;

    Queue<std::tuple<Pin<HttpRequest>, Pin<HttpResponse>>> request_queue_;

    Watch<usize> active_connections_;

    std::atomic<usize> max_connections_;

    std::atomic<i32> connection_timeout_ms_;

    Task task_;
};

}  // namespace batt

#endif  // BATTERIES_HTTP_HTTP_CLIENT_HOST_CONTEXT_DECL_HPP
