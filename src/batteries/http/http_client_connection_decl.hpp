//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_CLIENT_CONNECTION_DECL_HPP
#define BATTERIES_HTTP_HTTP_CLIENT_CONNECTION_DECL_HPP

#include <batteries/config.hpp>
//
#include <batteries/http/http_data.hpp>
#include <batteries/http/http_request.hpp>
#include <batteries/http/http_response.hpp>

#include <batteries/async/buffer_source.hpp>
#include <batteries/async/queue.hpp>
#include <batteries/async/stream_buffer.hpp>
#include <batteries/async/watch.hpp>

#include <batteries/asio/deadline_timer.hpp>
#include <batteries/asio/ip_tcp.hpp>
#include <batteries/status.hpp>

namespace batt {

class HttpClientHostContext;

class HttpClientConnection
{
   public:
    /** \brief A summary of information about a response message that is necessary for the connection to
     * correctly handle it.
     */
    struct ResponseInfo {
        explicit ResponseInfo(const pico_http::Response& response);

        bool is_valid() const
        {
            return this->content_length || this->chunked_encoding || !this->keep_alive;
        }

        HttpData get_data(StreamBuffer& input_buffer);

        //+++++++++++-+-+--+----- --- -- -  -  -   -

        /** \brief Set from the `Content-Length` header, if present.
         */
        Optional<usize> content_length;

        /** \brief Set to true if `Connection: keep-alive` was set, or if the HTTP version is 1.1 or later and
         * there is no explicit `Connection: close` header.
         */
        bool keep_alive;

        /** \brief Set to true if `Transfer-Encoding: chunked` is present.
         */
        bool chunked_encoding;
    };

    /** \brief Used to start an HttpClientConnection processing Task.
     *
     * The created Task will pull from the request/response pair queue of the passed context until an error
     * occurs or the queue is closed.  This function increments `active_connections` by one before returning,
     * and decrements `active_connections` when it exits.
     *
     * WARNING: Be careful about asserting/assuming that `active_connections` will appear to increase from the
     * caller's perspective!  This function guarantees that `active_connections` is incremented before
     * returning, but it does NOT guarantee that `active_connections` is not subsequently decremented "before"
     * returning, since that can happen asynchronously on some other thread/task.)
     *
     * This function uses batt::Task::spawn internally, so the created Task will be a sub-Task of the
     * currently running Task (this means that the task that calls HttpClientConnection::spawn will
     * automatically join to the sub-Task.
     *
     * It is the caller's responsibility to ensure that `context` and `active_connections` do not go out of
     * scope while there are still active connections (as tracked by the passed Watch).
     */
    static void spawn(HttpClientHostContext& context, Watch<usize>& active_connections) noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    explicit HttpClientConnection(HttpClientHostContext& context) noexcept;

    void run();

    Status process_requests();

    Status fill_input_buffer();

    Status process_responses();

    Status open_connection();

    void halt() noexcept;

    StatusOr<i32> read_next_response(pico_http::Response& response);

    boost::asio::io_context& get_io_context();

    StatusOr<boost::asio::ip::tcp::endpoint> local_endpoint() const noexcept;

    StatusOr<boost::asio::ip::tcp::endpoint> remote_endpoint() const noexcept;

    void idle_connection_timeout_task_main();

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    HttpClientHostContext& context_;

    i32 idle_connection_timeout_ms_;

    boost::asio::ip::tcp::socket socket_;

    boost::asio::deadline_timer idle_timer_;

    Queue<Pin<HttpResponse>> response_queue_;

    StreamBuffer input_buffer_{16 * 1024};

    Watch<usize> response_count_{0};

    Watch<u64> activity_{0};
};

}  // namespace batt

#endif  // BATTERIES_HTTP_HTTP_CLIENT_CONNECTION_DECL_HPP
