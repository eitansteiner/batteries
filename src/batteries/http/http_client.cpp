//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/http/http_client.hpp>
//
#include <batteries/http/http_client_impl.hpp>

#endif  // !BATT_HEADER_ONLY
