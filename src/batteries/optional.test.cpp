//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#include <batteries/optional.hpp>
//
#include <batteries/optional.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/stream_util.hpp>

namespace {

using namespace batt::int_types;

TEST(Optional, Test)
{
    batt::Optional<i64> a;
    batt::Optional<i32> b;

    b = a;

    EXPECT_EQ(b, batt::None);

    a = 7;
    b = a;

    EXPECT_TRUE(b);
    EXPECT_EQ(*b, 7);
}

TEST(Optional, TestOptionalOfRef)
{
    std::string s = "hello";
    std::string t = "there";

    {
        batt::Optional<std::string&> opt;

        EXPECT_FALSE(opt);
        EXPECT_FALSE(opt.has_value());
        EXPECT_EQ(opt, batt::None);
        EXPECT_EQ(batt::None, opt);
        EXPECT_THAT(batt::to_string(opt), ::testing::StrEq("--"));
    }
    {
        batt::Optional<std::string&> opt = s;

        ASSERT_TRUE(opt);
        EXPECT_TRUE(opt.has_value());
        EXPECT_NE(opt, batt::None);
        EXPECT_NE(batt::None, opt);
        EXPECT_EQ(s, *opt);
        EXPECT_EQ(*opt, s);
        EXPECT_EQ(&s, &*opt);
        EXPECT_EQ(&*opt, &s);
        EXPECT_THAT(batt::to_string(opt), ::testing::StrEq("hello"));

        opt = batt::None;

        EXPECT_FALSE(opt);
        EXPECT_FALSE(opt.has_value());
        EXPECT_EQ(opt, batt::None);
        EXPECT_EQ(batt::None, opt);
        EXPECT_THAT(batt::to_string(opt), ::testing::StrEq("--"));

        opt = s;

        ASSERT_TRUE(opt);
        EXPECT_TRUE(opt.has_value());
        EXPECT_NE(opt, batt::None);
        EXPECT_NE(batt::None, opt);
        EXPECT_EQ(s, *opt);
        EXPECT_EQ(*opt, s);
        EXPECT_EQ(&s, &*opt);
        EXPECT_EQ(&*opt, &s);
        EXPECT_THAT(batt::to_string(opt), ::testing::StrEq("hello"));

        opt = batt::None;

        EXPECT_FALSE(opt);
        EXPECT_FALSE(opt.has_value());
        EXPECT_EQ(opt, batt::None);
        EXPECT_EQ(batt::None, opt);
        EXPECT_THAT(batt::to_string(opt), ::testing::StrEq("--"));

        std::string& r = opt.emplace(t);

        EXPECT_EQ(&r, &t);
        ASSERT_TRUE(opt);
        EXPECT_TRUE(opt.has_value());
        EXPECT_NE(opt, batt::None);
        EXPECT_NE(batt::None, opt);
        EXPECT_EQ(t, *opt);
        EXPECT_EQ(*opt, t);
        EXPECT_EQ(&t, &*opt);
        EXPECT_EQ(&*opt, &t);
        EXPECT_THAT(batt::to_string(opt), ::testing::StrEq("there"));
        EXPECT_THAT(opt->c_str(), ::testing::StrEq("there"));
        EXPECT_EQ(opt->length(), 5u);
    }
}

// Test Plan (for Optional::or_else):
//
//  - For both T (1) and T& (2):
//     a. call or_else with non-None; fn not called, expected value returned.
//     b. call or_else with None; fn called, expected value returned.
//
TEST(Optional, TestOrElse)
{
    // 1a. T, non-None
    {
        bool called = false;
        batt::Optional<int> opt = 5;

        ASSERT_TRUE(opt);

        int ans = opt.or_else([&called] {
            called = true;
            return 7;
        });

        EXPECT_EQ(ans, 5);
        EXPECT_FALSE(called);
    }

    // 1b. T, None
    {
        bool called = false;
        batt::Optional<int> opt;

        EXPECT_FALSE(opt);

        int ans = opt.or_else([&called] {
            called = true;
            return 7;
        });

        EXPECT_EQ(ans, 7);
        EXPECT_TRUE(called);
    }

    // 2a. T&, non-None
    {
        bool called = false;
        int three = 3, four = 4;
        batt::Optional<int&> opt = three;

        ASSERT_TRUE(opt);

        int& ans = opt.or_else([&called, &four]() -> int& {
            called = true;
            return four;
        });

        EXPECT_EQ(&ans, &three);
        EXPECT_FALSE(called);
    }

    // 2b. T&, None
    {
        bool called = false;
        int two = 2;
        batt::Optional<int&> opt;

        EXPECT_FALSE(opt);

        int& ans = opt.or_else([&called, &two]() -> int& {
            called = true;
            return two;
        });

        EXPECT_EQ(&ans, &two);
        EXPECT_TRUE(called);
    }
}

}  // namespace
