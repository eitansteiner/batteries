//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/static_assert.hpp>
//
#include <batteries/static_assert.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

namespace {

// Uncomment to enable "DNC" (Does Not Compile) tests.
//
//#define BATT_COMPILE_DNC_TESTS

TEST(StaticAssert, Test)
{
    // Basic test; 2 + 3 is an int.
    //
    int x = BATT_CHECK_TYPE(int, 2 + 3);
    EXPECT_EQ(x, 5);

    // Check that BATT_CHECK_TYPE can be used with an rvalue expression without needing an explicit
    // std::forward/std::move.
    //
    std::unique_ptr<int> p_x = BATT_CHECK_TYPE(std::unique_ptr<int>, std::make_unique<int>(7));
    ASSERT_NE(p_x, nullptr);
    EXPECT_EQ(*p_x, 7);

#ifdef BATT_COMPILE_DNC_TESTS
    [[maybe_unused]] std::unique_ptr<int> p_x2 = BATT_CHECK_TYPE(int*, std::make_unique<int>(7));
#endif  // BATT_COMPILE_DNC_TESTS
}

}  // namespace
