//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/endian.hpp>
//
#include <batteries/endian.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace {

using namespace batt::int_types;

TEST(Endian, Test)
{
    const char a[] = "0000000012345678";
    const char b[] = "0000000023456789";

    EXPECT_FALSE(batt::big_endian_less_than(*(const u16*)(&a[6]), *(const u16*)(&b[6])));
    EXPECT_TRUE(batt::big_endian_less_than(*(const u16*)(&a[7]), *(const u16*)(&b[7])));
    EXPECT_FALSE(batt::big_endian_less_than(*(const u16*)(&b[7]), *(const u16*)(&a[7])));
    EXPECT_TRUE(batt::big_endian_less_than(*(const u16*)(&a[8]), *(const u16*)(&b[8])));
    EXPECT_FALSE(batt::big_endian_less_than(*(const u16*)(&b[8]), *(const u16*)(&a[8])));

    EXPECT_FALSE(batt::big_endian_less_than(*(const u32*)(&a[4]), *(const u32*)(&b[4])));
    EXPECT_TRUE(batt::big_endian_less_than(*(const u32*)(&a[5]), *(const u32*)(&b[5])));
    EXPECT_FALSE(batt::big_endian_less_than(*(const u32*)(&b[5]), *(const u32*)(&a[5])));
    EXPECT_TRUE(batt::big_endian_less_than(*(const u32*)(&a[6]), *(const u32*)(&b[6])));
    EXPECT_FALSE(batt::big_endian_less_than(*(const u32*)(&b[6]), *(const u32*)(&a[6])));

    EXPECT_FALSE(batt::big_endian_less_than(*(const u64*)(&a[0]), *(const u64*)(&b[0])));
    EXPECT_TRUE(batt::big_endian_less_than(*(const u64*)(&a[1]), *(const u64*)(&b[1])));
    EXPECT_FALSE(batt::big_endian_less_than(*(const u64*)(&b[1]), *(const u64*)(&a[1])));
    EXPECT_TRUE(batt::big_endian_less_than(*(const u64*)(&a[2]), *(const u64*)(&b[2])));
    EXPECT_FALSE(batt::big_endian_less_than(*(const u64*)(&b[2]), *(const u64*)(&a[2])));
}

}  // namespace
