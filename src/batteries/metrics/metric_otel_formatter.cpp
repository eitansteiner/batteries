//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/metrics/metric_otel_formatter.hpp>
//
#include <batteries/metrics/metric_otel_formatter_impl.hpp>

#endif  // !BATT_HEADER_ONLY
