//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi, Eitan Steiner
//
#pragma once
#ifndef BATTERIES_METRICS_METRIC_COLLECTORS_HPP
#define BATTERIES_METRICS_METRIC_COLLECTORS_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/int_types.hpp>

#include <algorithm>
#include <atomic>
#include <chrono>
#include <functional>
#include <ostream>

namespace batt {

template <typename T>
class CountMetric
{
   public:
    CountMetric() = default;

    /*implicit*/ CountMetric(T init_val) noexcept : value_{init_val}
    {
    }

    void set(T value)
    {
        this->value_.store(value, std::memory_order_relaxed);
    }

    template <typename D>
    void add(D delta)
    {
        this->value_.fetch_add(delta, std::memory_order_relaxed);
    }

    template <typename D>
    decltype(auto) fetch_add(D delta)
    {
        return this->value_.fetch_add(delta, std::memory_order_relaxed);
    }

    operator T() const
    {
        return this->value_;
    }

    decltype(auto) operator++(int)
    {
        return this->value_++;
    }

    decltype(auto) operator++()
    {
        return ++this->value_;
    }

    template <typename D>
    decltype(auto) operator+=(D delta)
    {
        return this->value_ += delta;
    }

    T load() const
    {
        return value_.load(std::memory_order_relaxed);
    }

    void reset()
    {
        this->value_.store(0, std::memory_order_relaxed);
    }

    /*! \brief Atomically ensure that value_ is at least lower_bound
     *  \param Lower bound */
    void clamp_min(T lower_bound)
    {
        T observed = this->value_.load();
        while (observed < lower_bound) {
            if (value_.compare_exchange_weak(observed, lower_bound)) {
                break;
            }
        }
    }

    /*! \brief Atomically ensure that value_ is at most upper_bound
     *  \param Upper bound */
    void clamp_max(T upper_bound)
    {
        T observed = this->value_.load();
        while (observed > upper_bound) {
            if (value_.compare_exchange_weak(observed, upper_bound)) {
                break;
            }
        }
    }

   private:
    std::atomic<T> value_{0};
};

class LatencyMetric
{
   public:
    void update(std::chrono::steady_clock::time_point start, u64 count_delta = 1)
    {
        return this->update(std::chrono::steady_clock::now() - start, count_delta);
    }

    void update(std::chrono::steady_clock::duration elapsed_duration, u64 count_delta = 1)
    {
        const i64 elapsed_usec =
            std::max<i64>(0, std::chrono::duration_cast<std::chrono::microseconds>(elapsed_duration).count());

        this->total_usec.add(elapsed_usec);
        this->count.add(count_delta);
    }

    // Count per second.
    //
    double rate_per_second() const
    {
        return double(count) / double(total_usec) * 1000.0 * 1000.0;
    }

    void reset()
    {
        this->total_usec.reset();
        this->count.reset();
    }

    CountMetric<u64> total_usec{0};
    CountMetric<u64> count{0};
};

inline std::ostream& operator<<(std::ostream& out, const LatencyMetric& t)
{
    //  return out << t.total_usec << "/" << t.count << "(avg=" << ((double)t.total_usec /
    //  (double)(t.count + 1))
    //         << "usec)";
    return out << ((double)t.total_usec / (double)(t.count + 1)) << "us(n=" << t.count << ")";
}

class LatencyTimer
{
   public:
    LatencyTimer(const LatencyTimer&) = delete;
    LatencyTimer& operator=(const LatencyTimer&) = delete;

    explicit LatencyTimer(LatencyMetric& counter, u64 delta = 1) noexcept : metric_{&counter}, delta_{delta}
    {
    }

    ~LatencyTimer() noexcept
    {
        this->stop();
    }

    i64 read_usec() const
    {
        return std::max<i64>(0, std::chrono::duration_cast<std::chrono::microseconds>(
                                    std::chrono::steady_clock::now() - this->start_)
                                    .count());
    }

    void stop() noexcept
    {
        if (this->metric_) {
            this->metric_->update(this->start_, this->delta_);
            this->metric_ = nullptr;
        }
    }

   private:
    LatencyMetric* metric_;
    const u64 delta_;
    const std::chrono::steady_clock::time_point start_ = std::chrono::steady_clock::now();
};

#define BATT_COLLECT_LATENCY(metric, expr)                                                                   \
    [&] {                                                                                                    \
        ::batt::LatencyTimer ANONYMOUS_latency_timer_{(metric)};                                             \
        return expr;                                                                                         \
    }()

#define BATT_COLLECT_LATENCY_N(metric, expr, count_delta)                                                    \
    [&] {                                                                                                    \
        ::batt::LatencyTimer ANONYMOUS_latency_timer_{(metric), (count_delta)};                              \
        return expr;                                                                                         \
    }()

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

template <typename T>
using DerivedMetric = std::function<T()>;

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
template <typename T, i64 kIntervalSeconds>
class RateMetric
{
   public:
    static const auto& base_time()
    {
        static const auto base_time_ = std::chrono::steady_clock::now();

        return base_time_;
    }

    void update(T value)
    {
        this->current_value_.store(value);

        const i64 now_usec = std::chrono::duration_cast<std::chrono::microseconds>(
                                 std::chrono::steady_clock::now() - base_time())
                                 .count();

        const i64 elapsed_usec = now_usec - this->start_time_;

        if (elapsed_usec >= kIntervalSeconds * 1000 * 1000 * 2) {
            this->start_time_.fetch_add(elapsed_usec / 2);
            this->start_value_.store((value + this->start_value_.load()) / 2);
        }
    }

    double get() const
    {
        const auto now_usec = std::chrono::duration_cast<std::chrono::microseconds>(
                                  std::chrono::steady_clock::now() - base_time())
                                  .count();

        const auto elapsed_usec = now_usec - this->start_time_;

        return static_cast<double>(this->current_value_ - this->start_value_) * 1000000.0 /
               static_cast<double>(elapsed_usec);
    }

   private:
    std::atomic<i64> start_time_{
        std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - base_time())
            .count()};
    std::atomic<T> start_value_{0};
    std::atomic<T> current_value_;
};

/*! \brief A Metric collector that stores and reports a single instantaneous value. */
template <typename T>
class GaugeMetric
{
   public:
    /*! \brief Initializes an empty metric. */
    GaugeMetric() = default;

    /*! \brief Sets the stored value to a given sample.
     *  \param sample value. */
    template <typename D>
    void set(D sample) noexcept
    {
        this->value_.store(sample, std::memory_order_relaxed);
    }

    /*! \return The gauge value. */
    T load() const noexcept
    {
        return this->value_.load(std::memory_order_relaxed);
    }

   private:
    std::atomic<T> value_{0};
};

/*! \brief Collect count, total, max and min values for multiple samples */
template <typename T>
class StatsMetric
{
   public:
    /*! \brief Initialize empty metric */
    StatsMetric() = default;

    /*! \brief Initialize non-empty metric
     *  \param Initial value */
    explicit StatsMetric(T init_val) noexcept : count_{1}, total_{init_val}, max_{init_val}, min_{init_val}
    {
    }

    /*! \brief Reset metric to empty state */
    void reset()
    {
        this->count_.reset();
        this->total_.reset();
        this->max_.set(std::numeric_limits<T>::min());
        this->min_.set(std::numeric_limits<T>::max());
    }

    /*! \brief Update count, total, min and max values for a given sample
     *  \param Sample value */
    template <typename D>
    void update(D sample)
    {
        this->count_.fetch_add(1);
        this->total_.fetch_add(sample);
        this->max_.clamp_min(sample);
        this->min_.clamp_max(sample);
    }

    /*! \return Number of samples */
    T count() const
    {
        return this->count_.load();
    }

    /*! \return Sum of samples */
    T total() const
    {
        return this->total_.load();
    }

    /*! \return Max sample */
    T max() const
    {
        return this->max_.load();
    }

    /*! \return Min sample */
    T min() const
    {
        return this->min_.load();
    }

   private:
    CountMetric<T> count_{0};
    CountMetric<T> total_{0};
    CountMetric<T> max_{std::numeric_limits<T>::min()};
    CountMetric<T> min_{std::numeric_limits<T>::max()};

    friend class MetricRegistry;
};

/*! \brief Collect histogram values for multiple samples
 *
 * Histogram initialization: provide data-range (min, max] and the number of buckets.
 * Histogram calculation: update per observed sample, uses constant memory.
 *
 * Buckets are evenly spaced, except when the required range/number of buckets ratio would result with a
 * remainder. Each bucket interval is: (lower-bound, upper-bound] with 2 excess buckets to account for
 * out-of-range samples, (which exceed expected maximum or subceed expected minimum values).
 * */
template <typename T>
class HistogramMetric
{
   public:
    /*! \brief The maximum number of intervals allowed in histogram initialization. */
    static constexpr batt::int_types::i64 kMaxNumberOfIntervals = /*-inf...*/ 1 + 16 + /*...+inf*/ 1;

    /*! \brief Represents a single histogram bucket for reporting. */
    struct Bucket {
        Bucket() = delete;

        explicit Bucket(T upper, T count, T total) noexcept : upper{upper}, count{count}, total{total}
        {
        }

        /*! \brief Bucket's upper-bound. */
        const T upper;
        /*! \brief Bucket's counted number of samples. */
        const T count;
        /*! \brief Bucket's total sum of samples. */
        const T total;
    };

    HistogramMetric() = default;

    HistogramMetric(const HistogramMetric&) = delete;

    HistogramMetric& operator=(const HistogramMetric&) = delete;

    /*! \brief Initializes an empty histogram with fixed number of buckets.
     *  Exits if initialization failed.
     *  \param intervals The fixed number of buckets.
     *  \param min_val   The expected minimum value.
     *  \param max_val   The expected maximum value. */
    HistogramMetric(const batt::int_types::usize intervals, const T min_val, const T max_val)
    {
        BATT_CHECK(initialize(intervals, min_val, max_val)) << "Histogram initialization failed";
    }

    /*! \brief Initializes an empty histogram with fixed number of buckets.
     *  Should be called in a single threaded context.
     *  \param intervals The fixed number of buckets.
     *  \param min_val   The expected minimum value.
     *  \param max_val   The expected maximum value.
     *  \return True if succeeded. */
    [[nodiscard]] bool initialize(const batt::int_types::usize intervals, const T min_val, const T max_val)
    {
        static_assert(std::is_integral<T>::value);
        this->reset();
        if (intervals < 1 || intervals > (kMaxNumberOfIntervals - 2) || min_val >= max_val) {
            return false;
        }
        this->num_intervals_ = static_cast<T>(intervals);
        this->min_val_ = min_val;
        this->max_val_ = max_val;
        // Add (divisor - 1) to emulate ceiling function.
        this->per_bucket_range_ = (this->overall_range() + this->num_intervals_ - 1) / this->num_intervals_;
        if (this->num_intervals_ > this->overall_range()) {
            this->reset();
            return false;
        }
        return true;
    }

    /*! \brief Resets metric to empty pre-initialized state.
     *  Should be called in a single threaded context. */
    void reset()
    {
        this->clear();
        this->num_intervals_ = 0u;
        this->min_val_ = 0;
        this->max_val_ = 0;
        this->per_bucket_range_ = 0;
    }

    /*! \brief Clears metric to empty post-initialized state.
     *  Should be called in a single threaded context. */
    void clear()
    {
        for (auto& bucket : this->buckets_) {
            bucket.reset();
        }
    }

    /*! \brief Finds index of correponding bucket for a given input sample.
     *         Thread-safe read-only access to scalar data members.
     *  \param sample value.
     *  \return The corresponding bucket: buckets_ index. */
    inline batt::int_types::usize bucket_index(T sample) const noexcept
    {
        BATT_CHECK_NE(this->num_intervals_, 0u) << "Histogram was not initialized";
        BATT_CHECK_NE(this->overall_range(), 0) << "Histogram was not initialized";

        return [&] {
            if (BATT_HINT_FALSE(sample > this->max_val_)) {  // Exceeds max
                return this->num_intervals_ + 1;
            } else if (BATT_HINT_FALSE(sample <= this->min_val_)) {  // Subceeds min
                return 0;
            } else {  // Find index of interval in range. Add (divisor - 1) to emulate ceiling function.
                auto found = (this->num_intervals_ * (sample - this->min_val_) + this->overall_range() - 1) /
                             this->overall_range();
                if (sample <= this->upper_bound(found - 1)) {  // Adjust when interval rounded up
                    --found;
                }
                return found;
            }
        }();
    }

    /*! \brief Updates the histogram with a given sample.
     *  \param sample value. */
    void update(T sample)
    {
        const auto index = this->bucket_index(sample);
        if (index >= 0 && index < this->num_intervals_ + 2u) {
            this->buckets_[index].update(sample);
        }
    }

    /*! \brief Gets a snapshot of histogram buckets current value.
     *  \return The buckets vector - each bucket element contains:
     *          upper-limit, count and total corresponding values.
     *          Empty vector if histogram was not initialized. */
    std::vector<HistogramMetric<T>::Bucket> get() const
    {
        std::vector<HistogramMetric<T>::Bucket> buckets;
        if (this->num_intervals_ > 0) {
            for (usize i = 0; i < this->num_intervals_ + 2u; ++i) {
                buckets.emplace_back(this->upper_bound(i), this->buckets_[i].count(),
                                     this->buckets_[i].total());
            }
        }
        return buckets;
    }

   private:
    T upper_bound(batt::int_types::usize interval) const noexcept
    {
        BATT_CHECK_NE(this->num_intervals_, 0u) << "Histogram was not initialized";
        BATT_CHECK_GE(this->num_intervals_ + 1u, interval) << "Histogram interval out of bounds";
        if (interval == 0) {
            return this->min_val_;
        } else if (interval == this->num_intervals_ + 1u) {
            return std::numeric_limits<T>::max();
        } else {
            const T upper_bound = this->min_val_ + this->per_bucket_range_ * static_cast<T>(interval);
            return std::min(upper_bound, this->max_val_);
        }
    }

    inline T overall_range() const noexcept
    {
        return (this->max_val_ - this->min_val_);
    }

    T num_intervals_{0};  // T instead of usize to minimize conversion during update-path arithmetic.
    T min_val_{0};
    T max_val_{0};
    T per_bucket_range_{0};  // Cache division result to mitigate repetition during update-path arithmetic.
    std::array<StatsMetric<T>, kMaxNumberOfIntervals> buckets_{};

    friend class MetricRegistry;
};

}  // namespace batt

#endif  // BATTERIES_METRICS_METRIC_COLLECTORS_HPP

#if BATT_HEADER_ONLY
#include <batteries/metrics/metric_collectors_impl.hpp>
#endif  // BATT_HEADER_ONLY
