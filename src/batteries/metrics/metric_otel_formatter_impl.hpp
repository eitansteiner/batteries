//#############################################################################
// Copyright 2023 Eitan Steiner
//
#pragma once
#ifndef BATTERIES_METRICS_METRIC_OTEL_FORMATTER_HPP
#define BATTERIES_METRICS_METRIC_OTEL_FORMATTER_HPP

#include <batteries/config.hpp>
//
#include <batteries/metrics/metric_otel_formatter.hpp>

#include <algorithm>
#include <vector>

namespace batt {

BATT_INLINE_IMPL void MetricOpenTelemetryFormatter::initialize(MetricRegistry& /*src*/, std::ostream& /*dst*/)
{
    // Nothing to do for OpenTelemtry format (stateless implementation).
}

BATT_INLINE_IMPL void MetricOpenTelemetryFormatter::format_values(MetricRegistry& src, std::ostream& dst)
{
    std::ostringstream buffer;
    std::vector<std::string> ordered_metrics;

    // Read current metrics in the registry.
    //
    src.read_all([&](std::string_view name, double value, const MetricLabelSet& labels) {
        buffer << name << " ";

        // Format optional labels.
        if (labels.size() > 0) {
            buffer << "{";
            for (const auto& label : labels) {
                buffer << label.key << "=\"" << label.value << "\",";
            }
            buffer.seekp(-1 /*overwrite last ,*/, std::ios_base::end);
            buffer << "}";
        }

        buffer << " " << std::setprecision(kMetricValuePrecision) << value << "\n";

        ordered_metrics.emplace_back(buffer.str());
        buffer.str("");
    });

    std::sort(ordered_metrics.begin(), ordered_metrics.end());

    for (const auto& om : ordered_metrics) {
        dst << om;
    }
}

BATT_INLINE_IMPL void MetricOpenTelemetryFormatter::finished(MetricRegistry& /*src*/, std::ostream& /*dst*/)
{
    // Nothing to do for OpenTelemtry format.
}

}  // namespace batt

#endif  // BATTERIES_METRICS_METRIC_OTEL_FORMATTER_HPP
