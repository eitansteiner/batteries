//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi, Eitan Steiner
//
#include <batteries/metrics/metric_collectors.hpp>
#include <batteries/metrics/metric_csv_formatter.hpp>
#include <batteries/metrics/metric_dumper.hpp>
#include <batteries/metrics/metric_formatter.hpp>
#include <batteries/metrics/metric_registry.hpp>
//
#include <batteries/metrics/metric_collectors.hpp>
#include <batteries/metrics/metric_csv_formatter.hpp>
#include <batteries/metrics/metric_dumper.hpp>
#include <batteries/metrics/metric_formatter.hpp>
#include <batteries/metrics/metric_registry.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#ifndef __clang__
#include <experimental/random>
#endif

#include <chrono>
#include <thread>

namespace {

//+++++++++++-+-+--+----- --- -- -  -  -   -
// Seems that libc++ (Clang) thread 'sleep' and timed synchronization primitive methods use system_clock
// instead of steady_clock; as a result, tests that use this_thread::sleep_for won't work quite right.  This
// is a workaround until Clang fixes this...
//
template <typename TimePoint, typename Duration>
void test_sleep(const TimePoint& start, const Duration& duration)
{
#if __clang__
    while ((std::chrono::steady_clock::now() - start) < duration) {
        std::this_thread::yield();
    }
#else
    std::this_thread::sleep_until(start + duration);
#endif
}
//+++++++++++-+-+--+----- --- -- -  -  -   -

TEST(Metrics, CounterMetricTest)
{
    batt::CountMetric count_i(0);
    static_assert(std::is_same_v<decltype(count_i), batt::CountMetric<int>>,
                  "Template argument should be deduced as int!");
    EXPECT_EQ(0, count_i++);
    EXPECT_EQ(2, ++count_i);
    EXPECT_EQ(4, count_i += 2);
    count_i.add(3);
    EXPECT_EQ(7, count_i.load());
    count_i.set(42);
    EXPECT_EQ(42, count_i.load());
    count_i.reset();
    EXPECT_EQ(0, count_i.load());
}

TEST(Metrics, CounterClampMinMaxTest)
{
    // Value updated by clamp_min
    batt::CountMetric<int> sample(0);
    sample.clamp_min(0);
    EXPECT_EQ(0, sample.load());
    sample.clamp_max(0);
    EXPECT_EQ(0, sample.load());
    for (int i = 1; i <= 10; i++) {
        sample.clamp_min(i);
        EXPECT_EQ(i, sample.load());
    }
    // Value updated by clamp_max
    sample.reset();
    sample.clamp_min(0);
    EXPECT_EQ(0, sample.load());
    sample.clamp_max(0);
    EXPECT_EQ(0, sample.load());
    for (int i = -1; i >= -10; i--) {
        sample.clamp_max(i);
        EXPECT_EQ(i, sample.load());
    }
    // Value not updated by clamp_min
    sample.reset();
    sample.clamp_min(0);
    EXPECT_EQ(0, sample.load());
    sample.clamp_max(0);
    EXPECT_EQ(0, sample.load());
    for (int i = -1; i >= -10; i--) {
        sample.clamp_min(i);
        EXPECT_EQ(0, sample.load());
    }
    // Value not updated by clamp_max
    sample.reset();
    sample.clamp_min(0);
    EXPECT_EQ(0, sample.load());
    sample.clamp_max(0);
    EXPECT_EQ(0, sample.load());
    for (int i = 1; i <= 10; i++) {
        sample.clamp_max(i);
        EXPECT_EQ(0, sample.load());
    }
    // Mixed order:
    sample.reset();
    sample.clamp_max(0);
    EXPECT_EQ(0, sample.load());
    sample.clamp_min(0);
    EXPECT_EQ(0, sample.load());
    sample.clamp_min(2);
    EXPECT_EQ(2, sample.load());
    sample.clamp_min(1);
    EXPECT_EQ(2, sample.load());
    sample.clamp_min(3);
    EXPECT_EQ(3, sample.load());
    sample.clamp_max(4);
    EXPECT_EQ(3, sample.load());
    sample.clamp_max(3);
    EXPECT_EQ(3, sample.load());
    sample.clamp_max(1);
    EXPECT_EQ(1, sample.load());
    sample.clamp_max(2);
    EXPECT_EQ(1, sample.load());
    sample.clamp_min(1);
    EXPECT_EQ(1, sample.load());
}

TEST(Metrics, LatencyMetricTest)
{
    const auto t10_msec = std::chrono::milliseconds(10);
    const auto start = std::chrono::steady_clock::now();
    batt::LatencyMetric lat;
    test_sleep(start, t10_msec);
    lat.update(start);  // count of 1 in 100msec
    const double actual_rate = lat.rate_per_second();
    const double expect_rate = 100;                                                 // 1/0.010s
    EXPECT_THAT(actual_rate, testing::DoubleNear(expect_rate, 0.4 * expect_rate));  // 40% for robustness
    lat.reset();
    {
        std::ostringstream oss;
        oss << lat;
        EXPECT_STREQ("0us(n=0)", oss.str().c_str());
    }
    lat.update(t10_msec, 2);
    EXPECT_EQ(200, lat.rate_per_second());  // Synthetic, exact match
}

TEST(Metrics, LatencyTimerTest)
{
    batt::LatencyMetric lat;
    std::ostringstream oss;
    batt::DerivedMetric<bool> sleep_for_10_ms = []() {
        test_sleep(std::chrono::steady_clock::now(), std::chrono::milliseconds(10));
        return true;
    };
    const bool expr_result = BATT_COLLECT_LATENCY(lat, sleep_for_10_ms());
    EXPECT_TRUE(expr_result);
    const double actual_rate = lat.rate_per_second();
    const double expect_rate = 100;                                                 // 1/0.010s
    EXPECT_THAT(actual_rate, testing::DoubleNear(expect_rate, 0.4 * expect_rate));  // 40% for robustness
}

TEST(Metrics, RateMetricTest)
{
    const auto t10_msec = std::chrono::milliseconds(10);
    batt::RateMetric<int, 0> rate;
    rate.update(1);
    test_sleep(std::chrono::steady_clock::now(), t10_msec);
    rate.update(3);
    const double actual_rate = rate.get();
    const double expect_rate = 400;                                                 // (1+3)/0.010s
    EXPECT_THAT(actual_rate, testing::DoubleNear(expect_rate, 0.4 * expect_rate));  // 40% for robustness
}

TEST(Metrics, GaugeMetricInt64Test)
{
    const batt::int_types::i64 zero_value = 0;
    const batt::int_types::i64 positive_value = 7;
    const batt::int_types::i64 negative_value = -42;
    batt::GaugeMetric<batt::int_types::i64> gauge;
    EXPECT_EQ(zero_value, gauge.load());
    gauge.set(positive_value);
    EXPECT_EQ(positive_value, gauge.load());
    gauge.set(negative_value);
    EXPECT_EQ(negative_value, gauge.load());
}

TEST(Metrics, GaugeMetricDoubleTest)
{
    const double zero = 0.0;
    const double pi = 3.14159;
    const double euler = 2.71828;
    batt::GaugeMetric<double> gauge;
    EXPECT_EQ(zero, gauge.load());
    gauge.set(pi);
    EXPECT_EQ(pi, gauge.load());
    gauge.set(euler);
    EXPECT_EQ(euler, gauge.load());
}

TEST(Metrics, GaugeRegistryTest)
{
    const batt::MetricLabel label{batt::Token("job"), batt::Token("batteries")};
    const batt::int_types::u64 zero_value = 0;
    const batt::int_types::u64 first_value = 26;
    const batt::int_types::u64 second_value = 18;
    batt::GaugeMetric<batt::int_types::u64> gauge;
    EXPECT_EQ(zero_value, gauge.load());
    gauge.set(first_value);
    EXPECT_EQ(first_value, gauge.load());
    gauge.set(second_value);
    EXPECT_EQ(second_value, gauge.load());

    batt::MetricRegistry& registry = ::batt::global_metric_registry();
    registry.add("test_ping", gauge, batt::MetricLabelSet{label});
    auto on_test_exit = batt::finally([&] {
        registry.remove(gauge);
    });

    const std::string header("id,time_usec,date_time,test_ping_gauge_job_batteries\n");

    std::ostringstream oss;
    batt::MetricCsvFormatter csv;
    csv.initialize(registry, oss);
    EXPECT_THAT(oss.str(), testing::StrEq(header));

    csv.format_values(registry, oss);
    const auto& actual = oss.str();
    EXPECT_THAT(actual, testing::StartsWith(header + "1,"));
    EXPECT_THAT(actual, testing::EndsWith(/* skip time_usec,date_time timestamps */ ",18\n"));
}

TEST(Metrics, BasicRegistryTest)
{
    const batt::MetricLabel label_1{batt::Token("instance"), batt::Token("localhost")};
    const batt::MetricLabel label_2{batt::Token("job"), batt::Token("batteries")};

    batt::CountMetric<double> counter;
    batt::LatencyMetric latency;
    counter.set(3.14);
    latency.update(std::chrono::microseconds(10000), 42);

    batt::MetricRegistry& registry = ::batt::global_metric_registry();
    registry.add("test_pi_counter", counter, batt::MetricLabelSet{label_1, label_2});
    registry.add("test_pi_latency", latency, batt::MetricLabelSet{label_1, label_2});
    auto on_test_exit = batt::finally([&] {
        registry.remove(counter);
        registry.remove(latency);
    });

    const std::string header(
        "id,time_usec,date_time,test_pi_counter_instance_localhost_job_batteries,test_pi_latency_count_"
        "instance_localhost_job_batteries,test_pi_latency_total_usec_instance_localhost_job_batteries\n");

    std::ostringstream oss;
    batt::MetricCsvFormatter csv;
    csv.initialize(registry, oss);
    EXPECT_THAT(oss.str(), testing::StrEq(header));

    csv.format_values(registry, oss);
    const auto& actual = oss.str();
    EXPECT_THAT(actual, testing::StartsWith(header + "1,"));
    EXPECT_THAT(actual, testing::EndsWith(/* skip time_usec,date_time timestamps */ ",3.14,42,10000\n"));
}

TEST(Metrics, SanitizedCsvFormatterTest)
{
    // Introduce labels which include ',' chars in key and value token strings.
    const batt::MetricLabel label_1{batt::Token("inst,ance,"), batt::Token("lo,cal,host")};
    const batt::MetricLabel label_2{batt::Token(",job"), batt::Token("batt,eries")};
    const batt::MetricLabel label_3{batt::Token("c,,o,,,m,,,,"), batt::Token(",")};

    batt::CountMetric<int> counter;
    batt::MetricRegistry& registry = ::batt::global_metric_registry();

    registry.add("counter_with_sanitized_labels", counter, batt::MetricLabelSet{label_1, label_2, label_3});
    auto on_test_exit = batt::finally([&] {
        registry.remove(counter);
    });

    // Check that original ',' chars were replaced with '_' in the formatted CSV header.
    const std::string header(
        "id,time_usec,date_time,counter_with_sanitized_labels__job_batt_eries_c__o___m_______inst_ance__lo_cal_host\n");

    std::ostringstream oss;
    batt::MetricCsvFormatter csv;

    csv.initialize(registry, oss);
    EXPECT_THAT(oss.str(), testing::StrEq(header));
}

}  // namespace
