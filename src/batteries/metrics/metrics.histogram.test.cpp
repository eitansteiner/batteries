//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi, Eitan Steiner
//
#include <batteries/metrics/metric_collectors.hpp>
#include <batteries/metrics/metric_csv_formatter.hpp>
#include <batteries/metrics/metric_dumper.hpp>
#include <batteries/metrics/metric_formatter.hpp>
#include <batteries/metrics/metric_registry.hpp>
//
#include <batteries/metrics/metric_collectors.hpp>
#include <batteries/metrics/metric_csv_formatter.hpp>
#include <batteries/metrics/metric_dumper.hpp>
#include <batteries/metrics/metric_formatter.hpp>
#include <batteries/metrics/metric_registry.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#ifndef __clang__
#include <experimental/random>
#endif

#include <chrono>
#include <thread>

namespace {

TEST(MetricsHistogram, BasicLogicTest)
{
    batt::HistogramMetric<int> histogram;
    const int max_end = std::numeric_limits<int>::max();

    // Negtive - incorrect input
    //
    EXPECT_FALSE(histogram.initialize(0, 1, 2));
    EXPECT_FALSE(histogram.initialize(-1, 0, 2));
    EXPECT_FALSE(histogram.initialize(17, 0, 1));
    EXPECT_FALSE(histogram.initialize(1, 2, 0));
    EXPECT_FALSE(histogram.initialize(2, 0, -1));

    // Negative - incorrect spacing
    //
    EXPECT_FALSE(histogram.initialize(2, 0, 1));
    EXPECT_FALSE(histogram.initialize(17, -2, 1));

    // Positive - edge spacing
    //
    EXPECT_TRUE(histogram.initialize(1, 0, 1));
    std::vector<batt::HistogramMetric<int>::Bucket> intervals = histogram.get();
    EXPECT_EQ(3u, intervals.size());
    EXPECT_EQ(0, intervals[0].upper);
    EXPECT_EQ(1, intervals[1].upper);
    EXPECT_EQ(max_end, intervals[2].upper);
    EXPECT_TRUE(histogram.initialize(2, -2, 0));
    intervals = histogram.get();
    EXPECT_EQ(4u, intervals.size());
    EXPECT_EQ(-2, intervals[0].upper);
    EXPECT_EQ(-1, intervals[1].upper);
    EXPECT_EQ(0, intervals[2].upper);
    EXPECT_EQ(max_end, intervals[3].upper);
    EXPECT_TRUE(histogram.initialize(1, -2, 1));
    intervals = histogram.get();
    EXPECT_EQ(3u, intervals.size());
    EXPECT_EQ(-2, intervals[0].upper);
    EXPECT_EQ(1, intervals[1].upper);
    EXPECT_EQ(max_end, intervals[2].upper);

    // Positive - equal spacing
    //
    int bins = 2;
    int max_val = 200;
    int min_val = 100;
    int width = 50;

    EXPECT_TRUE(histogram.initialize(bins, min_val, max_val));

    int interval[] = {min_val, min_val + width, max_val, max_end};
    const std::vector<batt::HistogramMetric<int>::Bucket> empty_buckets = histogram.get();
    EXPECT_EQ(4u, empty_buckets.size());
    for (int i = 0; i < bins + 2; i++) {
        const auto& ebi = empty_buckets[i];
        EXPECT_EQ(interval[i], ebi.upper);
        EXPECT_EQ(0, ebi.count);
        EXPECT_EQ(0, ebi.total);
    }
    EXPECT_EQ(3u, histogram.bucket_index(max_end));
    histogram.update(max_end);
    EXPECT_EQ(3u, histogram.bucket_index(max_val + 1));
    histogram.update(max_val + 1);
    EXPECT_EQ(2u, histogram.bucket_index(max_val));
    histogram.update(max_val);
    EXPECT_EQ(2u, histogram.bucket_index(max_val + 1 - width));
    histogram.update(max_val + 1 - width);
    EXPECT_EQ(1u, histogram.bucket_index(min_val + width));
    histogram.update(min_val + width);
    EXPECT_EQ(1u, histogram.bucket_index(min_val + 1));
    histogram.update(min_val + 1);
    EXPECT_EQ(0u, histogram.bucket_index(min_val));
    histogram.update(min_val);
    EXPECT_EQ(0u, histogram.bucket_index(std::numeric_limits<int>::min()));
    histogram.update(std::numeric_limits<int>::min());
    const std::vector<batt::HistogramMetric<int>::Bucket> buckets = histogram.get();
    EXPECT_EQ(bins + 2, static_cast<int>(buckets.size()));
    for (int i = 0; i < bins + 1; i++) {
        EXPECT_EQ(2, buckets[i].count);
        const int upper = min_val + width * i;
        EXPECT_EQ(upper, buckets[i].upper);
    }
    EXPECT_EQ(2, buckets[bins + 1].count);
    EXPECT_EQ(max_end, buckets[bins + 1].upper);

    // Positive - clear (empty each bucket)
    histogram.clear();
    EXPECT_EQ(4u, histogram.get().size());
    for (const auto& bucket : histogram.get()) {
        EXPECT_EQ(0, bucket.count);
        EXPECT_EQ(0, bucket.total);
    }
}

TEST(MetricsHistogram, InitializationDeathTest)
{
    // Negtive - incorrect input
    //
    EXPECT_DEATH(batt::HistogramMetric<int> histogram(0, 1, 2), "Histogram initialization failed");
    EXPECT_DEATH(batt::HistogramMetric<int> histogram(-1, 0, 2), "Histogram initialization failed");
    EXPECT_DEATH(batt::HistogramMetric<int> histogram(17, 0, 1), "Histogram initialization failed");
    EXPECT_DEATH(batt::HistogramMetric<int> histogram(1, 2, 0), "Histogram initialization failed");
    EXPECT_DEATH(batt::HistogramMetric<int> histogram(2, 0, -1), "Histogram initialization failed");

    // Negative - incorrect spacing
    //
    EXPECT_DEATH(batt::HistogramMetric<int> histogram(2, 0, 1), "Histogram initialization failed");
    EXPECT_DEATH(batt::HistogramMetric<int> histogram(17, -2, 1), "Histogram initialization failed");

    // Negative - uninitialized histogram
    //
    batt::HistogramMetric<int> histogram;
    histogram.reset();

    EXPECT_DEATH(histogram.bucket_index(0), "Histogram was not initialized");
    EXPECT_DEATH(histogram.bucket_index(std::numeric_limits<int>::min()), "Histogram was not initialized");
    EXPECT_DEATH(histogram.bucket_index(std::numeric_limits<int>::max()), "Histogram was not initialized");
}

TEST(MetricsHistogram, ExtendedLogicTest)
{
    const int max_end = std::numeric_limits<int>::max();

    // Positive - intervals around zero
    //
    int bins = 5;
    int max_val = 500;
    int min_val = -250;
    int width = 150;
    batt::HistogramMetric<int> histogram(bins, min_val, max_val);
    std::vector<batt::HistogramMetric<int>::Bucket> intervals = histogram.get();

    EXPECT_EQ(7u, intervals.size());
    EXPECT_EQ(0u, histogram.bucket_index(std::numeric_limits<int>::min()));
    for (int i = 0; i < bins; i++) {
        EXPECT_EQ(min_val + width * i, intervals[i].upper);
        EXPECT_EQ(static_cast<batt::int_types::usize>(i), histogram.bucket_index(min_val + width * i));
        EXPECT_EQ(static_cast<batt::int_types::usize>(i + 1),
                  histogram.bucket_index(min_val + 1 + width * i));
    }
    EXPECT_EQ(max_val, intervals[bins].upper);
    EXPECT_EQ(static_cast<batt::int_types::usize>(bins), histogram.bucket_index(max_val));
    EXPECT_EQ(max_end, intervals[bins + 1].upper);
    EXPECT_EQ(static_cast<batt::int_types::usize>(bins + 1), histogram.bucket_index(max_val + 1));
    EXPECT_EQ(static_cast<batt::int_types::usize>(bins + 1), histogram.bucket_index(max_end));

    // Positive - narrower bin
    //
    histogram.reset();
    bins = 3;
    max_val = 1300;
    min_val = 300;
    width = 334;  // 333.333

    EXPECT_TRUE(histogram.initialize(bins, min_val, max_val));

    intervals = histogram.get();
    EXPECT_EQ(5u, intervals.size());
    EXPECT_EQ(0u, histogram.bucket_index(std::numeric_limits<int>::min()));
    for (int i = 0; i < bins; i++) {
        EXPECT_EQ(min_val + width * i, intervals[i].upper);
        EXPECT_EQ(static_cast<batt::int_types::usize>(i), histogram.bucket_index(min_val + width * i));
        EXPECT_EQ(static_cast<batt::int_types::usize>(i + 1),
                  histogram.bucket_index(min_val + 1 + width * i));
    }
    EXPECT_EQ(max_val, intervals[bins].upper);
    EXPECT_EQ(static_cast<batt::int_types::usize>(bins), histogram.bucket_index(max_val));
    EXPECT_EQ(max_end, intervals[bins + 1].upper);
    EXPECT_EQ(static_cast<batt::int_types::usize>(bins + 1), histogram.bucket_index(max_val + 1));
    EXPECT_EQ(static_cast<batt::int_types::usize>(bins + 1), histogram.bucket_index(max_end));
    histogram.reset();
    max_val = -300;
    min_val = -1300;
    EXPECT_TRUE(histogram.initialize(bins, min_val, max_val));
    intervals = histogram.get();
    EXPECT_EQ(0u, histogram.bucket_index(std::numeric_limits<int>::min()));
    for (int i = 0; i < bins; i++) {
        EXPECT_EQ(min_val + width * i, intervals[i].upper);
        EXPECT_EQ(static_cast<batt::int_types::usize>(i), histogram.bucket_index(min_val + width * i));
        EXPECT_EQ(static_cast<batt::int_types::usize>(i + 1),
                  histogram.bucket_index(min_val + 1 + width * i));
    }
    EXPECT_EQ(max_val, intervals[bins].upper);
    EXPECT_EQ(static_cast<batt::int_types::usize>(bins), histogram.bucket_index(max_val));
    EXPECT_EQ(max_end, intervals[bins + 1].upper);
    EXPECT_EQ(static_cast<batt::int_types::usize>(bins + 1), histogram.bucket_index(max_val + 1));
    EXPECT_EQ(static_cast<batt::int_types::usize>(bins + 1), histogram.bucket_index(max_end));

    // Positive - reset
    //
    histogram.reset();
    EXPECT_TRUE(histogram.get().empty());
}

TEST(MetricsHistogram, BasicConcurrentTest)
{
    // 1st run - Table histogram
    //
    // Initial state:
    //
    int bins = 4;
    int max_val = 100;
    int min_val = 0;
    batt::HistogramMetric<int> histogram(bins, min_val, max_val);

    EXPECT_EQ(bins + 2, static_cast<int>(histogram.get().size()));

    // Concurrent updates:
    //
    auto run_update = [&histogram](int from, int to, int step) {
        for (int i = from; (step > 0 ? i <= to : i >= to); i += step) {
            histogram.update(i);
        }
    };
    std::thread t1(run_update, 52, 100, 2);  // even - up
    std::thread t2(run_update, 50, 2, -2);   // even - down
    std::thread t3(run_update, 51, 99, 2);   // odd  - up
    std::thread t4(run_update, 49, 1, -2);   // odd  - down
    t1.join();
    t2.join();
    t3.join();
    t4.join();

    // Final state:
    //
    std::vector<batt::HistogramMetric<int>::Bucket> buckets = histogram.get();
    EXPECT_EQ(bins + 2, static_cast<int>(buckets.size()));

    const auto& b0 = buckets[0];
    EXPECT_EQ(0, b0.upper);
    EXPECT_EQ(0, b0.count);
    EXPECT_EQ(0, b0.total);
    const auto& b5 = buckets[5];
    EXPECT_EQ(std::numeric_limits<int>::max(), b5.upper);
    EXPECT_EQ(0, b5.count);
    EXPECT_EQ(0, b5.total);
    const int width = (max_val - min_val) / bins;
    for (int i = 1; i <= bins; i++) {
        const auto& bi = buckets[i];
        EXPECT_EQ(width * i, bi.upper);
        EXPECT_EQ(width, bi.count);
        // Sn = n⁄2 {2a + (n − 1) d}
        const int a = (1 + width * (i - 1));
        const int d = 1;
        const int bin_sum = width * (2 * a + (width - 1) * d) / 2;
        EXPECT_EQ(bin_sum, bi.total);
    }

    // 2nd run - Pyramid histogram
    //
    // Initial state:
    //
    bins = 5;
    EXPECT_TRUE(histogram.initialize(bins, min_val, max_val));
    const std::vector<batt::HistogramMetric<int>::Bucket> empty_buckets = histogram.get();
    for (int i = 0; i < bins + 2; i++) {
        const auto& ebi = empty_buckets[i];
        EXPECT_EQ(0, ebi.count);
        EXPECT_EQ(0, ebi.total);
    }

    // Concurrent updates:
    //
    std::thread t5(run_update, -1, 50, 1);
    std::thread t6(run_update, 21, 80, 1);
    std::thread t7(run_update, 60, 41, -1);
    std::thread t8(run_update, 51, 102, 1);
    t5.join();
    t6.join();
    t7.join();
    t8.join();

    // Final state:
    //
    buckets = histogram.get();
    EXPECT_EQ(7u, buckets.size());

    const auto& b0_2nd = buckets[0];
    EXPECT_EQ(0, b0_2nd.upper);
    EXPECT_EQ(2, b0_2nd.count);
    EXPECT_EQ(-1, b0_2nd.total);
    const auto& b1 = buckets[1];
    EXPECT_EQ(20, b1.upper);
    EXPECT_EQ(20, b1.count);
    EXPECT_EQ(210, b1.total);
    const auto& b2 = buckets[2];
    EXPECT_EQ(40, b2.upper);
    EXPECT_EQ(40, b2.count);
    EXPECT_EQ(1220, b2.total);
    const auto& b3 = buckets[3];
    EXPECT_EQ(60, b3.upper);
    EXPECT_EQ(60, b3.count);
    EXPECT_EQ(3030, b3.total);
    const auto& b4 = buckets[4];
    EXPECT_EQ(80, b4.upper);
    EXPECT_EQ(40, b4.count);
    EXPECT_EQ(2820, b4.total);
    const auto& b5_2nd = buckets[5];
    EXPECT_EQ(100, b5_2nd.upper);
    EXPECT_EQ(20, b5_2nd.count);
    EXPECT_EQ(1810, b5_2nd.total);
    const auto& b6 = buckets[6];
    EXPECT_EQ(std::numeric_limits<int>::max(), b6.upper);
    EXPECT_EQ(2, b6.count);
    EXPECT_EQ(203, b6.total);
    histogram.reset();
}

TEST(MetricsHistogram, RegistryTest)
{
    // Initial state:
    //
    batt::HistogramMetric<int> histogram(2, 0, 3);
    batt::MetricRegistry& registry = ::batt::global_metric_registry();
    const batt::MetricLabel label{batt::Token("job"), batt::Token("batt")};

    // Register and update histogram:
    //
    registry.add("test", histogram, batt::MetricLabelSet{label});
    auto on_test_exit = batt::finally([&] {
        registry.remove(histogram);
    });
    for (int i = 0; i <= 4; i++) {
        histogram.update(i);
    }

    // Expected CSV header:
    //
    const std::string header(
        "id,time_usec,date_time,test_histogram_bucket_count_job_batt_le_0,test_histogram_bucket_count_job_"
        "batt_le_2,test_histogram_bucket_count_job_batt_le_3,test_histogram_bucket_count_job_batt_le_Inf,"
        "test_histogram_bucket_max_job_batt_le_0,test_histogram_bucket_max_job_batt_le_2,test_histogram_"
        "bucket_max_job_batt_le_3,test_histogram_bucket_max_job_batt_le_Inf,test_histogram_bucket_min_job_"
        "batt_le_0,test_histogram_bucket_min_job_batt_le_2,test_histogram_bucket_min_job_batt_le_3,test_"
        "histogram_bucket_min_job_batt_le_Inf,test_histogram_bucket_total_job_batt_le_0,test_histogram_"
        "bucket_total_job_batt_le_2,test_histogram_bucket_total_job_batt_le_3,test_histogram_bucket_total_"
        "job_batt_le_Inf\n");

    // Check initial CSV header line:
    //
    std::ostringstream oss;
    batt::MetricCsvFormatter csv;
    csv.initialize(registry, oss);
    EXPECT_THAT(oss.str(), testing::StrEq(header));

    // Check final formatted data:
    //
    csv.format_values(registry, oss);
    const auto& actual = oss.str();
    EXPECT_THAT(actual, testing::StartsWith(header + "1,"));
    EXPECT_THAT(actual, testing::EndsWith(
                            /* skip time_usec,date_time timestamps */ ",1,2,1,1,0,2,3,4,0,1,3,4,0,3,3,4\n"));
}

TEST(MetricsHistogram, UpdateBenchmarkTest)
{
    constexpr int kNumberOfUpdates = 1000000;
    constexpr int kNumberOfBuckets = 16;
    batt::HistogramMetric<int> histogram(kNumberOfBuckets, 0, kNumberOfUpdates);
    batt::LatencyMetric latency;
    int actual_updates = 0;

    // Lambda to benchmark:
    //
    batt::DerivedMetric<bool> update_histogram = [&histogram, &actual_updates]() {
        for (int i = 1; i <= kNumberOfUpdates; ++i) {
            histogram.update(i);
            ++actual_updates;
        }
        return (actual_updates == kNumberOfUpdates);
    };
    const bool ran_to_completion = BATT_COLLECT_LATENCY(latency, update_histogram());

    // Check results:
    //
    EXPECT_TRUE(ran_to_completion);

    const double observed_rate = latency.rate_per_second();
    // observed rate ~40..200 [Mega-updates/sec] (target dependent)
    std::cout << "Observed rate: " << observed_rate << " [Mega-updates/sec]" << std::endl;

    // Final state:
    //
    const auto buckets = histogram.get();
    EXPECT_EQ(18u, buckets.size());

    // Bucket 0
    EXPECT_EQ(0, buckets[0].upper);
    EXPECT_EQ(0, buckets[0].count);

    // Buckets 1..16
    constexpr int kExpectedUpdatesPerBucket = kNumberOfUpdates / kNumberOfBuckets;
    for (int i = 1; i <= kNumberOfBuckets; i++) {
        EXPECT_EQ(kExpectedUpdatesPerBucket * i, buckets[i].upper);
        EXPECT_EQ(kExpectedUpdatesPerBucket, buckets[i].count);
    }
    // Bucket 17
    EXPECT_EQ(std::numeric_limits<int>::max(), buckets[17].upper);
    EXPECT_EQ(0, buckets[17].count);
}

}  // namespace
