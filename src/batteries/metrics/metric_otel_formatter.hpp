//#############################################################################
// Copyright 2023 Eitan Steiner
//
#pragma once
#ifndef BATTERIES_METRIC_OTEL_FORMATTER_HPP
#define BATTERIES_METRIC_OTEL_FORMATTER_HPP

#include <batteries/config.hpp>
//
#include <batteries/metrics/metric_formatter.hpp>
#include <batteries/metrics/metric_registry.hpp>

namespace batt {

/*! \brief Format metrics into OpenTelemetry data format:
 * <metric name>{<label name>=<label value>, ...} <metric value>
 *
 * Example:
 * `http_requests_total{method="POST", job="Batteries"} 36000`
 *
 * Usage:
 * Call `format_values` any number of times to format the current metric values stored in `src` to `dst`.
 * `format_values` implementation is stateless and does not require calling `initialize` / `finished`.
 * Note that `dst` is not flushed (and it's left up to the caller to decide when to perform this action).
 *
 * Output to `dst` is reported in lexicographic order to maintain consistency between calls.
 * Since it's possible for metrics to be added or removed from `src` over time, the data reported to `dst`
 * represents a snapshot of the current metrics and values during `format_values` call time.
 *
 * */
class MetricOpenTelemetryFormatter : public MetricFormatter
{
   public:
    /*! \brief The maximum precision for reporting metric numeric value to `dst`. */
    static constexpr int kMetricValuePrecision = 10 /* digits */;

    /*! \brief Nothing to do for OpenTelemtry format (stateless implementation). */
    void initialize(MetricRegistry& src, std::ostream& dst) override;

    /*! \brief Format the current metrics and values stored in `src` to `dst`.
     *  \param src  The MetricRegistry where metrics and values are stored.
     *  \param dst  The output stream to where formatted data is reported.
     *  */
    void format_values(MetricRegistry& src, std::ostream& dst) override;

    /*! \brief Nothing to do for OpenTelemtry format (stateless implementation). */
    void finished(MetricRegistry& src, std::ostream& dst) override;
};

}  // namespace batt

#endif  // BATTERIES_METRIC_OTEL_FORMATTER_HPP

#if BATT_HEADER_ONLY
#include <batteries/metrics/metric_otel_formatter_impl.hpp>
#endif  // BATT_HEADER_ONLY
