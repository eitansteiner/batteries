//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/metrics/metric_dumper.hpp>
//
#include <batteries/metrics/metric_dumper_impl.hpp>

#endif  // !BATT_HEADER_ONLY
