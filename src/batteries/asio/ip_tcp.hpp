#pragma once
#ifndef BATTERIES_ASIO_IP_TCP_HPP
#define BATTERIES_ASIO_IP_TCP_HPP

#include <batteries/suppress.hpp>

BATT_SUPPRESS_IF_GCC("-Wsuggest-override")
BATT_SUPPRESS_IF_CLANG("-Wsuggest-override")
//
#include <boost/asio/ip/tcp.hpp>
//
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_GCC()

#endif  // BATTERIES_ASIO_IP_TCP_HPP
