//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASIO_EXECUTION_CONTEXT_HPP
#define BATTERIES_ASIO_EXECUTION_CONTEXT_HPP

#include <batteries/suppress.hpp>

BATT_SUPPRESS_IF_GCC("-Wsuggest-override")
BATT_SUPPRESS_IF_GCC("-Woverloaded-virtual")
BATT_SUPPRESS_IF_CLANG("-Wsuggest-override")
//
#include <boost/asio/execution_context.hpp>
//
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_GCC()
BATT_UNSUPPRESS_IF_GCC()

#endif  // BATTERIES_ASIO_EXECUTION_CONTEXT_HPP
