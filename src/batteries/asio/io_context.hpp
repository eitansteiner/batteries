//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASIO_IO_CONTEXT_HPP
#define BATTERIES_ASIO_IO_CONTEXT_HPP

#include <batteries/suppress.hpp>

BATT_SUPPRESS_IF_GCC("-Wsuggest-override")
//
#include <boost/asio/io_context.hpp>
//
BATT_UNSUPPRESS_IF_GCC()

#endif  // BATTERIES_ASIO_IO_CONTEXT_HPP
