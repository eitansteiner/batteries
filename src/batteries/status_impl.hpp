//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_STATUS_IMPL_HPP
#define BATTERIES_STATUS_IMPL_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/config.hpp>

#include <utility>
#include <vector>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL const char* Status::CodeGroup::name() const noexcept
{
    return name_of(this->enum_type_index);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ const std::string& Status::unknown_enum_value_message()
{
    static const std::string s = "(Unknown enum value; not registered via batt::Status::register_codes)";
    return s;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ const Status::CodeEntry& Status::get_entry_from_code(value_type value)
{
    const usize index_of_group = Status::get_index_of_group(value);
    const usize index_within_group = Status::get_index_within_group(value);
    const auto& all_groups = Status::registered_groups();

    BATT_CHECK_LT(index_of_group, all_groups.size());
    BATT_CHECK_LT(index_within_group, all_groups[index_of_group]->entries.size())
        << BATT_INSPECT(index_of_group) << BATT_INSPECT(value);

    return all_groups[index_of_group]->entries[index_within_group];
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ std::string_view Status::message_from_code(value_type value)
{
    return Status::get_entry_from_code(value).message;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status::Status() : Status(StatusCode::kOk)
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool Status::ok() const noexcept BATT_WARN_UNUSED_RESULT
{
    return (this->value_ & kLocalMask) == 0;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status::value_type Status::code() const noexcept
{
    return this->value_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status::value_type Status::code_index_within_group() const noexcept
{
    return Status::get_index_within_group(this->value_);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL const Status::CodeEntry& Status::code_entry() const noexcept
{
    return Status::get_entry_from_code(this->value_);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL std::string_view Status::message() const noexcept
{
#ifdef BATT_STATUS_CUSTOM_MESSSAGES
    return this->message_;
#else
    return message_from_code(this->value_);
#endif
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL const Status::CodeGroup& Status::group() const
{
    const usize index_of_group = get_index_of_group(this->value_);
    const auto& all_groups = registered_groups();

    BATT_CHECK_LT(index_of_group, all_groups.size());
    BATT_ASSERT_NOT_NULLPTR(all_groups[index_of_group]);

    return *all_groups[index_of_group];
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Status::IgnoreError() const noexcept
{
    // do nothing
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Status::Update(const Status& new_status)
{
    if (this->ok() || *this == Status{StatusCode::kUnknown}) {
        *this = new_status;
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ usize Status::next_group_index()
{
    static std::atomic<i32> next_index{0};
    const usize i = next_index.fetch_add(1);
    BATT_CHECK_LT(i, kMaxGroupCount);
    return i;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ std::array<Status::CodeGroup*, Status::kMaxGroupCount>&
Status::registered_groups()
{
    static std::array<CodeGroup*, kMaxGroupCount> all_groups;
    [[maybe_unused]] static bool initialized = [] {
        all_groups.fill(nullptr);
        return true;
    }();

    return all_groups;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ usize Status::get_index_of_group(value_type value)
{
    return (value & kGroupMask) >> kGroupSizeBits;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ usize Status::get_index_within_group(value_type value)
{
    return value & kLocalMask;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL std::ostream& operator<<(std::ostream& out, const Status& t)
{
    return out << t.code() << ":" << t.message();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool operator==(const Status& l, const Status& r)
{
    return l.code() == r.code() || (l.ok() && r.ok());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool operator!=(const Status& l, const Status& r)
{
    return !(l == r);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status OkStatus()
{
    return Status{};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL detail::StatusBase::StatusBase() noexcept
{
    [[maybe_unused]] static bool initialized = [] {
        Status::register_codes_internal<StatusCode>({
            {StatusCode::kOk, "Ok"},
            {StatusCode::kCancelled, "Cancelled"},
            {StatusCode::kUnknown, "Unknown"},
            {StatusCode::kInvalidArgument, "Invalid Argument"},
            {StatusCode::kDeadlineExceeded, "Deadline Exceeded"},
            {StatusCode::kNotFound, "Not Found"},
            {StatusCode::kAlreadyExists, "Already Exists"},
            {StatusCode::kPermissionDenied, "Permission Denied"},
            {StatusCode::kResourceExhausted, "Resource Exhausted"},
            {StatusCode::kFailedPrecondition, "Failed Precondition"},
            {StatusCode::kAborted, "Aborted"},
            {StatusCode::kOutOfRange, "Out of Range"},
            {StatusCode::kUnimplemented, "Unimplemented"},
            {StatusCode::kInternal, "Internal"},
            {StatusCode::kUnavailable, "Unavailable"},
            {StatusCode::kDataLoss, "Data Loss"},
            {StatusCode::kUnauthenticated, "Unauthenticated"},
            //
            {StatusCode::kClosed, "Closed"},
            {StatusCode::kGrantUnavailable, "The requested grant count exceeds available count"},
            {StatusCode::kLoopBreak, "Loop break"},
            {StatusCode::kEndOfStream, "End of stream"},
            {StatusCode::kClosedBeforeEndOfStream, "The stream was closed before the end of data"},
            {StatusCode::kGrantRevoked, "The Grant was revoked"},
            {StatusCode::kPoke, "The observed object was poked"},
        });

        std::vector<std::pair<ErrnoValue, std::string>> errno_codes;
        for (int code = 0; code < Status::kGroupSize; ++code) {
            const char* msg = std::strerror(code);
            if (msg) {
                errno_codes.emplace_back(static_cast<ErrnoValue>(code), std::string(msg));
            } else {
                errno_codes.emplace_back(static_cast<ErrnoValue>(code), std::string("(unknown)"));
            }
        }
        return Status::register_codes_internal<ErrnoValue>(errno_codes);
    }();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool is_ok_status(const std::error_code& ec)
{
    return !ec;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL std::ostream& operator<<(std::ostream& out, LogLevel t)
{
    switch (t) {
    case LogLevel::kFatal:
        return out << "LogLevel::kFatal";

    case LogLevel::kError:
        return out << "LogLevel::kError";

    case LogLevel::kWarning:
        return out << "LogLevel::kWarning";

    case LogLevel::kInfo:
        return out << "LogLevel::kInfo";

    case LogLevel::kDebug:
        return out << "LogLevel::kDebug";

    case LogLevel::kVerbose:
        return out << "LogLevel::kVerbose";

    default:
        break;
    }
    return out << "(Bad LogLevel value:" << (int)t << ")";
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL std::atomic<LogLevel>& require_fail_global_default_log_level()
{
    static std::atomic<LogLevel> global_default_{LogLevel::kVerbose};
    return global_default_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Optional<LogLevel>& require_fail_thread_default_log_level()
{
    thread_local Optional<LogLevel> per_thread_default_;
    return per_thread_default_;
}

namespace detail {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ NotOkStatusWrapper::NotOkStatusWrapper(usize this_size, const char* file,
                                                                     int line, Status&& status,
                                                                     bool vlog_is_on) noexcept
    : file_{file}
    , line_{line}
    , status_(std::move(status))
    , vlog_is_on_{vlog_is_on}
{
    BATT_CHECK_EQ(sizeof(NotOkStatusWrapper), this_size);
    *this << this->status_ << "; ";
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ NotOkStatusWrapper::NotOkStatusWrapper(usize this_size, const char* file,
                                                                     int line, const Status& status,
                                                                     bool vlog_is_on) noexcept
    : file_{file}
    , line_{line}
    , status_(status)
    , vlog_is_on_{vlog_is_on}
{
    BATT_CHECK_EQ(sizeof(NotOkStatusWrapper), this_size);
#if BATT_WITH_GLOG
    *this << "(" << this->file_ << ":" << this->line_ << ") " << this->status_ << "; ";
#endif  // BATT_WITH_GLOG
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL NotOkStatusWrapper::~NotOkStatusWrapper() noexcept
{
#if BATT_WITH_GLOG
    switch (this->level_) {
    case LogLevel::kFatal:
        ::google::LogMessage(this->file_, this->line_, google::GLOG_FATAL).stream() << this->output_.str();
        break;
    case LogLevel::kError:
        ::google::LogMessage(this->file_, this->line_, google::GLOG_ERROR).stream() << this->output_.str();
        break;
    case LogLevel::kWarning:
        ::google::LogMessage(this->file_, this->line_, google::GLOG_WARNING).stream() << this->output_.str();
        break;
    case LogLevel::kInfo:
        ::google::LogMessage(this->file_, this->line_, google::GLOG_INFO).stream() << this->output_.str();
        break;
    case LogLevel::kDebug:
        DLOG(INFO) << " [" << this->file_ << ":" << this->line_ << "] " << this->output_.str();
        break;
    case LogLevel::kVerbose:
        if (this->vlog_is_on_) {
            ::google::LogMessage(this->file_, this->line_, google::GLOG_INFO).stream() << this->output_.str();
        }
        break;
    }
#endif  // BATT_WITH_GLOG
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL NotOkStatusWrapper::operator Status() noexcept
{
    return std::move(this->status_);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL NotOkStatusWrapper& NotOkStatusWrapper::operator<<(LogLevel new_level)
{
    this->level_ = new_level;
    return *this;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL
std::unordered_map<const boost::system::error_category*, const std::unordered_map<int, Status>>&
thread_local_error_category_status_map()
{
    thread_local std::unordered_map<const boost::system::error_category*,
                                    const std::unordered_map<int, Status>>
        thread_cache_;

    return thread_cache_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL std::mutex& global_error_category_status_map_mutex()
{
    static std::mutex mutex_;

    return mutex_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL
std::unordered_map<const boost::system::error_category*, const std::unordered_map<int, Status>>&
global_error_category_status_map()
{
    static std::unordered_map<const boost::system::error_category*, const std::unordered_map<int, Status>>
        global_cache_;

    return global_cache_;
}

}  //namespace detail

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status status_from_error_category(const boost::system::error_category& category, int code)
{
    // First check in the thread-local cache for the given category.
    //
    auto& thread_cache = detail::thread_local_error_category_status_map();
    auto iter = thread_cache.find(&category);
    if (iter != thread_cache.end()) {
        //  The category was found in the local cache; look up the code value.
        //
        auto iter2 = iter->second.find(code);
        if (iter2 != iter->second.end()) {
            return iter2->second;
        }
    }

    // Fall back on the global cache.  To access this, we need to grab the mutex.
    {
        std::unique_lock<std::mutex> lock{detail::global_error_category_status_map_mutex()};

        auto& global_cache = detail::global_error_category_status_map();
        auto iter2 = global_cache.find(&category);
        if (iter2 != global_cache.end()) {
            //  We found the category in the global cache; add it to the thread cache and retry.
            //
            thread_cache.emplace(*iter2);
            return status_from_error_category(category, code);
        }
    }

    // This code wasn't registered for this category, or the category wasn't found.  Return `kUnknown`.
    //
    return StatusCode::kUnknown;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status status_from_error_code(const boost::system::error_code& ec)
{
    return status_from_error_category(ec.category(), ec.value());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status error_code_to_status(const boost::system::error_code& ec)
{
    if (!ec) {
        return OkStatus();
    }
    if (&(ec.category()) == &(boost::asio::error::get_misc_category())) {
        switch (ec.value()) {
        case boost::asio::error::eof:
            return Status{StatusCode::kEndOfStream};

        case boost::asio::error::not_found:
            return Status{StatusCode::kNotFound};

        default:
            break;
        }
    } else if (&(ec.category()) == &(boost::asio::error::get_system_category())) {
        switch (ec.value()) {
        case boost::asio::error::access_denied:
            return status_from_errno(EACCES);

        case boost::asio::error::address_family_not_supported:
            return status_from_errno(EAFNOSUPPORT);

        case boost::asio::error::address_in_use:
            return status_from_errno(EADDRINUSE);

        case boost::asio::error::already_connected:
            return status_from_errno(EISCONN);

        case boost::asio::error::already_started:
            return status_from_errno(EALREADY);

        case boost::asio::error::broken_pipe:
            // TODO [tastolfi 2022-12-16] On Windows: ERROR_BROKEN_PIPE
            return status_from_errno(EPIPE);

        case boost::asio::error::connection_aborted:
            return status_from_errno(ECONNABORTED);

        case boost::asio::error::connection_refused:
            return status_from_errno(ECONNREFUSED);

        case boost::asio::error::connection_reset:
            return status_from_errno(ECONNRESET);

        case boost::asio::error::bad_descriptor:
            return status_from_errno(EBADF);

        case boost::asio::error::fault:
            return status_from_errno(EFAULT);

        case boost::asio::error::host_unreachable:
            return status_from_errno(EHOSTUNREACH);

        case boost::asio::error::in_progress:
            return status_from_errno(EINPROGRESS);

        case boost::asio::error::interrupted:
            return status_from_errno(EINTR);

        case boost::asio::error::invalid_argument:
            return status_from_errno(EINVAL);

        case boost::asio::error::message_size:
            return status_from_errno(EMSGSIZE);

        case boost::asio::error::name_too_long:
            return status_from_errno(ENAMETOOLONG);

        case boost::asio::error::network_down:
            return status_from_errno(ENETDOWN);

        case boost::asio::error::network_reset:
            return status_from_errno(ENETRESET);

        case boost::asio::error::network_unreachable:
            return status_from_errno(ENETUNREACH);

        case boost::asio::error::no_descriptors:
            return status_from_errno(EMFILE);

        case boost::asio::error::no_buffer_space:
            return status_from_errno(ENOBUFS);

        case boost::asio::error::no_memory:
            // TODO [tastolfi 2022-12-16] Windows: ERROR_OUTOFMEMORY
            return status_from_errno(ENOMEM);

        case boost::asio::error::no_permission:
            // TODO [tastolfi 2022-12-16] Windows: ERROR_ACCESS_DENIED
            return status_from_errno(EPERM);

        case boost::asio::error::no_protocol_option:
            return status_from_errno(ENOPROTOOPT);

        case boost::asio::error::no_such_device:
            // TODO [tastolfi 2022-12-16] Windows: ERROR_BAD_UNIT
            return status_from_errno(ENODEV);

        case boost::asio::error::not_connected:
            return status_from_errno(ENOTCONN);

        case boost::asio::error::not_socket:
            return status_from_errno(ENOTSOCK);

        case boost::asio::error::operation_aborted:
            // TODO [tastolfi 2022-12-16] Windows: ERROR_OPERATION_ABORTED
            return status_from_errno(ECANCELED);

        case boost::asio::error::operation_not_supported:
            return status_from_errno(EOPNOTSUPP);

        case boost::asio::error::shut_down:
            return status_from_errno(ESHUTDOWN);

        case boost::asio::error::timed_out:
            return status_from_errno(ETIMEDOUT);

        case boost::asio::error::try_again:
            // TODO [tastolfi 2022-12-16] Windows: ERROR_RETRY
            return status_from_errno(EAGAIN);

#if !defined(__linux__) && !defined(__APPLE__)
#if EAGAIN != EWOULDBLOCK
        case boost::asio::error::would_block:
            return status_from_errno(EWOULDBLOCK);
#endif
#endif  // __linux__

        default:
            break;
        }
    }
    return status_from_error_code(ec);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool status_is_retryable(const Status& s)
{
    return s == StatusCode::kUnavailable            //
           || s == static_cast<ErrnoValue>(EAGAIN)  //
           || s == static_cast<ErrnoValue>(EINTR)   //
           || s == StatusCode::kPoke                //
        ;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status::Status(const StatusOr<NoneType>& status_or) noexcept : Status{status_or.status()}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status& Status::operator=(const StatusOr<NoneType>& status_or) noexcept
{
    *this = status_or.status();
    return *this;
}

}  // namespace batt

#endif  // BATTERIES_STATUS_IMPL_HPP
