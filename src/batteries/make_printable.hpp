//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_MAKE_PRINTABLE_HPP
#define BATTERIES_MAKE_PRINTABLE_HPP

#include <batteries/config.hpp>
//
#include <batteries/type_traits.hpp>
#include <batteries/utility.hpp>

#include <iomanip>
#include <sstream>
#include <string>

namespace batt {

/** \brief Returns a std::ostream-printable representation of the argument, `obj`.
 *
 * This overload is selected for types that support an overloaded `operator<<` for std::ostream.  It simply
 * forwards the argument as the return value.
 */
template <typename T, typename = std::enable_if_t<IsPrintable<T>{}>>
decltype(auto) make_printable(T&& obj)
{
    return BATT_FORWARD(obj);
}

/** \brief Returns a std::ostream-printable representation of the argument, `obj`.
 *
 * This overload is selected for all types that do _not_ support an overloaded `operator<<` for std::ostream.
 * It prints the name of the type (demangled), and a hex representation of the bytes of the object itself.
 */
template <typename T, typename = std::enable_if_t<!IsPrintable<T>{}>, typename = void>
std::string make_printable(T&& obj)
{
    std::ostringstream oss;
    oss << "(" << name_of<T>() << ") " << std::hex << std::setw(2) << std::setfill('0');

    for (const u8* bytes = (const u8*)&obj; bytes != (const u8*)((&obj) + 1); ++bytes) {
        oss << (int)*bytes;
    }
    return oss.str();
}

}  //namespace batt

#endif  // BATTERIES_MAKE_PRINTABLE_HPP
