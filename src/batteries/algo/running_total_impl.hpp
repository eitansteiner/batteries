//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ALGO_RUNNING_TOTAL_IMPL_HPP
#define BATTERIES_ALGO_RUNNING_TOTAL_IMPL_HPP

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ RunningTotal::RunningTotal(PartsCount count, PartSize size) : values_{nullptr}
{
    this->reset(count, size);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto RunningTotal::begin() const -> iterator
{
    return iterator{this, 0};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto RunningTotal::end() const -> iterator
{
    return iterator{this, this->size()};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto RunningTotal::slice(usize begin_index, usize end_index) const -> slice_type
{
    BATT_CHECK_LE(begin_index, end_index);
    return slice_type{
        std::next(this->begin(), begin_index),  //
        std::next(this->begin(), end_index)     //
    };
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto RunningTotal::slice(Interval<usize> interval) const -> slice_type
{
    return this->slice(interval.lower_bound, interval.upper_bound);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void RunningTotal::reset(PartsCount count, PartSize size)
{
    BATT_CHECK_GT(size, 0);

    try {
        std::swap(this->parts_count_, count);
        std::swap(this->part_size_, size);
        this->values_.reset(new usize[this->raw_size()]);

        this->mutable_summary().front() = 0;
        for (usize i = 0; i < this->parts_count_; ++i) {
            this->mutable_part(i).front() = 0;
        }

        this->size_ = this->parts_count_ * this->part_size_ + 1;
    } catch (...) {
        std::swap(this->parts_count_, count);
        std::swap(this->part_size_, size);
        throw;
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void RunningTotal::set_size(usize new_size)
{
    BATT_CHECK_LE(new_size, this->parts_count_ * this->part_size_ + 1);
    this->size_ = new_size;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Slice<const usize> RunningTotal::const_part(usize i) const
{
    BATT_ASSERT_LT(i, this->parts_count_) << BATT_INSPECT(this->size()) << BATT_INSPECT(this->raw_size());

    return this->const_part_impl(i);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Slice<usize> RunningTotal::mutable_part(usize i)
{
    BATT_ASSERT_LT(i, this->parts_count_) << BATT_INSPECT(this->size()) << BATT_INSPECT(this->raw_size());

    return this->raw_slice(this->part_segment_offset(i), this->part_segment_size());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Slice<const usize> RunningTotal::const_summary() const
{
    return this->raw_slice(this->summary_offset(), this->summary_size());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Slice<usize> RunningTotal::mutable_summary()
{
    return this->raw_slice(this->summary_offset(), this->summary_size());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize RunningTotal::operator[](isize i) const
{
    BATT_ASSERT_LT(static_cast<usize>(i), this->size());
    const isize part_index = i / this->part_size_;
    const isize part_offset = i % this->part_size_;
    return this->const_summary()[part_index] + this->const_part_impl(part_index)[part_offset];
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Slice<const usize> RunningTotal::raw_values() const
{
    return as_slice(this->values_.get(), this->raw_size());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL std::function<void(std::ostream&)> RunningTotal::dump() const
{
    return [this](std::ostream& out) {
        out << std::endl
            << "RunningTotal{" << std::endl
            << "  .parts_count=" << this->parts_count_ << "," << std::endl
            << "  .part_size=" << this->part_size_ << "," << std::endl
            << "  .raw_size=" << this->raw_size() << "," << std::endl
            << "  .size=" << this->size() << "," << std::endl
            << "  .values=" << dump_range(this->raw_values()) << std::endl
            << ",}   ==   " << dump_range(*this);
    };
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void RunningTotal::update_summary()
{
    usize total = 0;
    usize* next_summary = this->values_.get() + this->summary_offset();
    BATT_CHECK_EQ(*next_summary, 0u);
    ++next_summary;

    usize step = this->part_segment_size();
    usize* next_part_total = this->values_.get() + part_size_;
    usize* last_part_total = next_part_total + (step * this->parts_count());
    for (; next_part_total != last_part_total; next_part_total += step, ++next_summary) {
        total += *next_part_total;
        *next_summary = total;
    }

    BATT_CHECK_EQ(next_summary, this->values_.get() + this->raw_size()) << BATT_INSPECT(this->raw_size());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Slice<const usize> RunningTotal::const_part_impl(usize i) const
{
    const usize offset = this->part_segment_offset(i);
    const usize len = this->part_segment_size();
    BATT_ASSERT_LT(offset, this->raw_size()) << BATT_INSPECT(offset) << BATT_INSPECT(len);
    return this->raw_slice(offset, len);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Slice<const usize> RunningTotal::raw_slice(usize offset, usize size) const
{
    return as_slice(this->values_.get() + offset, size);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Slice<usize> RunningTotal::raw_slice(usize offset, usize size)
{
    return as_slice(this->values_.get() + offset, size);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize RunningTotal::raw_size() const
{
    return this->summary_offset() + this->summary_size();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize RunningTotal::part_segment_offset(usize part_i) const
{
    return part_i * this->part_segment_size();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize RunningTotal::part_segment_size() const
{
    return this->part_size_ + /*leading zero*/ 1;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize RunningTotal::summary_offset() const
{
    return this->part_segment_size() * this->parts_count();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize RunningTotal::summary_size() const
{
    return this->parts_count() + /*leading zero*/ 1;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize RunningTotal::offset_of_part(usize part_i) const
{
    return part_i * this->part_segment_size();
}

}  //namespace batt

#endif  // BATTERIES_ALGO_RUNNING_TOTAL_IMPL_HPP
