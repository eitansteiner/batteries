//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/algo/running_total.hpp>
//
#include <batteries/algo/running_total_impl.hpp>

#endif  // !BATT_HEADER_ONLY
