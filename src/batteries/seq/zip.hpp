//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_SEQ_ZIP_HPP
#define BATTERIES_SEQ_ZIP_HPP

#include <batteries/config.hpp>
//

#include <batteries/seq/seq_item.hpp>

#include <tuple>
#include <type_traits>

namespace batt {
namespace seq {

template <typename... Seqs>
class Zip
{
   public:
    using Item = std::tuple<SeqItem<Seqs>...>;

    explicit Zip(Seqs&&... seqs) noexcept : seqs_{BATT_FORWARD(seqs)...}
    {
    }

    Optional<Item> peek()
    {
        if (this->done_) {
            return None;
        }
        Optional<Item> item = this->peek_impl(std::integral_constant<usize, 0>{});
        if (!item) {
            this->done_ = true;
        }
        return item;
    }

    Optional<Item> next()
    {
        if (this->done_) {
            return None;
        }
        Optional<Item> item = this->next_impl(std::integral_constant<usize, 0>{});
        if (!item) {
            this->done_ = true;
        }
        return item;
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

   private:
    template <usize kIndex, typename... PrevItems>
    Optional<Item> peek_impl(std::integral_constant<usize, kIndex>, PrevItems&&... prev_items)
    {
        decltype(auto) next_item = std::get<kIndex>(this->seqs_).peek();
        if (!next_item) {
            return None;
        }
        return this->peek_impl(std::integral_constant<usize, kIndex + 1>{}, BATT_FORWARD(prev_items)...,
                               *BATT_FORWARD(next_item));
    }

    template <typename... AllItems>
    Optional<Item> peek_impl(std::integral_constant<usize, sizeof...(Seqs)>, AllItems&&... all_items)
    {
        return Item{BATT_FORWARD(all_items)...};
    }

    //----- --- -- -  -  -   -

    template <usize kIndex, typename... PrevItems>
    Optional<Item> next_impl(std::integral_constant<usize, kIndex>, PrevItems&&... prev_items)
    {
        decltype(auto) next_item = std::get<kIndex>(this->seqs_).next();
        if (!next_item) {
            return None;
        }
        return this->next_impl(std::integral_constant<usize, kIndex + 1>{}, BATT_FORWARD(prev_items)...,
                               *BATT_FORWARD(next_item));
    }

    template <typename... AllItems>
    Optional<Item> next_impl(std::integral_constant<usize, sizeof...(Seqs)>, AllItems&&... all_items)
    {
        return Item{BATT_FORWARD(all_items)...};
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    bool done_ = false;
    std::tuple<Seqs...> seqs_;
};

template <typename... OtherSeqs>
struct ZipBinder {
    std::tuple<OtherSeqs...> other_seqs;
};

template <typename... OtherSeqs>
ZipBinder<OtherSeqs&&...> zip(OtherSeqs&&... other_seqs)
{
    return {{BATT_FORWARD(other_seqs)...}};
}

namespace detail {

inline void zip_seqs_static_asserts()
{
}

template <typename First, typename... Rest>
inline void zip_seqs_static_asserts(StaticType<First>, StaticType<Rest>... rest)
{
    static_assert(std::is_same_v<First, std::decay_t<First>>,
                  "Zipped sequences may not be captured implicitly by reference.");

    zip_seqs_static_asserts(rest...);
}

}  //namespace detail

template <typename Seq, typename... OtherSeqs>
[[nodiscard]] Zip<Seq, OtherSeqs...> operator|(Seq&& seq, ZipBinder<OtherSeqs&&...>&& binder)
{
    detail::zip_seqs_static_asserts(StaticType<Seq>{}, StaticType<OtherSeqs>{}...);

    return std::apply(
        [&](auto&&... other_seqs) {
            return Zip<Seq, OtherSeqs...>{BATT_FORWARD(seq), BATT_FORWARD(other_seqs)...};
        },
        std::move(binder.other_seqs));
}

}  //namespace seq
}  // namespace batt

#endif  // BATTERIES_SEQ_ZIP_HPP
