//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/ref.hpp>
//
#include <batteries/ref.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/utility.hpp>

#include <string_view>
#include <type_traits>

namespace {

TEST(RefTest, UnwrapRef)
{
    std::string_view s = "hello, world!";

    batt::Ref<std::string_view> r{s};
    {
        EXPECT_TRUE((std::is_same_v<decltype(batt::unwrap_ref(r)), std::string_view&>));

        std::string_view& unwrap_s = batt::unwrap_ref(r);

        EXPECT_EQ(&s, &unwrap_s);
    }

    const batt::Ref<std::string_view>& r_const_ref = r;
    {
        EXPECT_TRUE((std::is_same_v<decltype(batt::unwrap_ref(r_const_ref)), std::string_view&>));

        std::string_view& unwrap_s = batt::unwrap_ref(r_const_ref);

        EXPECT_EQ(&s, &unwrap_s);
    }

    batt::Ref<std::string_view>&& r_rvalue_ref = std::move(r);
    {
        EXPECT_TRUE((std::is_same_v<decltype(batt::unwrap_ref(r_rvalue_ref)), std::string_view&>));

        std::string_view& unwrap_s = batt::unwrap_ref(r_rvalue_ref);

        EXPECT_EQ(&s, &unwrap_s);
    }

    const batt::Ref<std::string_view>&& r_const_rvalue_ref = std::move(r);
    {
        EXPECT_TRUE((std::is_same_v<decltype(batt::unwrap_ref(r_const_rvalue_ref)), std::string_view&>));

        std::string_view& unwrap_s = batt::unwrap_ref(r_const_rvalue_ref);

        EXPECT_EQ(&s, &unwrap_s);
    }
}

}  // namespace
