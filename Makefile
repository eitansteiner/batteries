##=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++
#
# Copyright 2023, Anthony Paul Astolfi
#
#+++++++++++-+-+--+----- --- -- -  -  -   -

export PROJECT_DIR := $(shell pwd)
PROJECT_NAME := batteries
SCRIPT_DIR := $(PROJECT_DIR)/script
CONAN_2 := 0

include $(SCRIPT_DIR)/conan-targets.mk
include $(SCRIPT_DIR)/doc-targets.mk

#=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
# Test target

#----- --- -- -  -  -   -
.PHONY: test
test:
	mkdir -p "$(BUILD_DIR)"
ifeq ("$(GTEST_FILTER)","")
	@echo -e "\n\nRunning DEATH tests ==============================================\n"
	(cd "$(BUILD_DIR)" && GTEST_OUTPUT='xml:$(BUILD_DIR)/death-test-results.xml' GTEST_FILTER='*Death*' ./BattTest)
	@echo -e "\n\nRunning non-DEATH tests ==========================================\n"
	(cd "$(BUILD_DIR)" && GTEST_OUTPUT='xml:$(BUILD_DIR)/test-results.xml' GTEST_FILTER='*-*Death*' ./BattTest)
else
	(cd "$(BUILD_DIR)" && GTEST_OUTPUT='xml:$(BUILD_DIR)/test-results.xml' ./BattTest)
endif

#=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
# Docker targets

# The list of OS/Compiler/Arch variants to build images for.
#
DOCKER_PLATFORM_VARIANTS := \
	linux_gcc11_amd64 \


DOCKER_IMAGE_PREFIX := registry.gitlab.com/batteriescpp/batteries
DOCKER_TAG_VERSION_PREFIX := v$(shell "$(SCRIPT_DIR)/get-version.sh")
DOCKER_TS_DIR := $(BUILD_DIR)/docker
DOCKER_TS_BUILD_LIST := $(foreach VARIANT,$(DOCKER_PLATFORM_VARIANTS),$(DOCKER_TS_DIR)/$(VARIANT).build.ts)
DOCKER_TS_PUSH_LIST := $(foreach VARIANT,$(DOCKER_PLATFORM_VARIANTS),$(DOCKER_TS_DIR)/$(VARIANT).push.ts)

$(info DOCKER_TS_BUILD_LIST is $(DOCKER_TS_BUILD_LIST))
$(info DOCKER_TS_PUSH_LIST is $(DOCKER_TS_PUSH_LIST))

#----- --- -- -  -  -   -
$(DOCKER_TS_DIR)/%.build.ts: $(PROJECT_DIR)/docker/Dockerfile.% $(MAKEFILE_LIST)
	mkdir -p "$(DOCKER_TS_DIR)"
	(cd "$(PROJECT_DIR)/docker" && docker build -t $(DOCKER_IMAGE_PREFIX):$(DOCKER_TAG_VERSION_PREFIX).$* -f Dockerfile.$* .)
	docker tag $(DOCKER_IMAGE_PREFIX):$(DOCKER_TAG_VERSION_PREFIX).$* $(DOCKER_IMAGE_PREFIX):latest.$*
	docker tag $(DOCKER_IMAGE_PREFIX):$(DOCKER_TAG_VERSION_PREFIX).$* $(DOCKER_IMAGE_PREFIX):latest
	echo "$(DOCKER_IMAGE_PREFIX):$(DOCKER_TAG_VERSION_PREFIX).$*" > "$@"
	touch "$@"


#----- --- -- -  -  -   -
.PHONY: docker-build
docker-build: $(DOCKER_TS_BUILD_LIST)

#----- --- -- -  -  -   -
$(DOCKER_TS_DIR)/%.push.ts: $(DOCKER_TS_DIR)/%.build.ts $(MAKEFILE_LIST)
	docker push $(DOCKER_IMAGE_PREFIX):$(DOCKER_TAG_VERSION_PREFIX).$*
	docker push $(DOCKER_IMAGE_PREFIX):latest.$*
	docker push $(DOCKER_IMAGE_PREFIX):latest
	touch "$@"

#----- --- -- -  -  -   -
.PHONY: docker-push
docker-push: $(DOCKER_TS_PUSH_LIST)

#----- --- -- -  -  -   -
.PHONY: docker
docker: docker-build docker-push
